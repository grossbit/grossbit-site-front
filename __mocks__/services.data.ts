const servicesData = [
    {
        name: 'IT-продукт под ключ',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
        icon: '/assets/key-product.png',
        link: '/services/product',
    },
    {
        name: 'Аутсорсинг',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
        icon: '/assets/outsource.png',
        link: '/services/outsouce',
    },
    {
        name: 'Спасти продукт',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
        icon: '/assets/save.png',
        link: '/services/product-saving',
    },
    {
        name: 'Модернизация ПО',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
        icon: '/assets/modern.png',
        link: '/services/product-upgrade',
    },
]

export default servicesData