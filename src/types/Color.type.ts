type Color = {
    r: number | string
    g: number | string
    b: number | string
    a?: number | string
}

export default Color
