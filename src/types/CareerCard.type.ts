type CareerCard = {
    id: string
    vacancy_id: number
    title: string
    geo_place: string
    department: string
}

export default CareerCard
