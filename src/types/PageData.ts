import { ArrayElements } from './Elements.type'

type PageData = {
    id: string
    rendering_data: {
        cover?: {
            title: string
            image: string
            tags: string[]
        }
        content: ArrayElements
    }
    pythia_properties?: {
        bgColor: string
        activeFontColor: string
    }
    header_properties?: {
        fontColor: string
        bgColor: string
        activeFontColor: string
    }
    body_properties?: {
        fontColor: string
        bgColor: string
    }
}

export default PageData
