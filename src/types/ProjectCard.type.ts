import Color from './Color.type'

type ProjectCard = {
    id: string
    case_id: number
    title: string
    tags: string[]
    size: 'small' | 'middle' | 'pre-middle' | 'large' | 'side' | 'side-middle'
    bg_image_url: string
    overlay_color: Color
    title_font_color: Color
    tag_bg_color: Color
    tag_font_color: Color
}

export default ProjectCard
