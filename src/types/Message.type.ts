export enum MessageAuthor {
    BOT = 'pythia',
    USER = 'user',
}

export type TextMessageData = { content: string }
export type FileMessageData = {
    media_unit: string
    media_url: string
    filename: string
    size: number | string
}

export type Message = {
    id: string
    data_type: 'text' | 'file' | 'callback'
    date: string | Date
    author: MessageAuthor
    data: TextMessageData | FileMessageData
    buttons?: {
        text: string
        callback: string
    }[]
}
