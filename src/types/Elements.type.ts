type ImageElement = {
    type: 'image'
    src: string
}

type TextElement = {
    type: 'text'
    text: string
}

type HeadingElement = {
    type: 'heading'
    text: string
}

type GridElement = {
    type: 'grid'
    content: Array<(ImageElement | TextElement) & { size: 'large' | 'medium' }>
}

type ListElement = {
    type: 'list'
    numeric?: boolean
    items: string[]
}

type Cover = {
    title: string
    tags?: string[]
    image: string
}

type ArrayElements = Array<
    ImageElement | TextElement | HeadingElement | GridElement | ListElement
>

export type {
    ImageElement,
    TextElement,
    HeadingElement,
    GridElement,
    Cover,
    ListElement,
    ArrayElements,
}
