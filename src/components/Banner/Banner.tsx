import React from 'react'
import { Box, Button, Flex } from '@chakra-ui/react'
import { BellRing, X } from 'lucide-react'
import BannerStyles from './Banner.module.scss'

const Banner: React.FC = () => {
    return (
        <Box className={BannerStyles['container']}>
            <Flex justifyContent="space-between" alignItems="center">
                <Flex alignItems="center">
                    <Box
                        background="#3730a3"
                        padding={3}
                        borderRadius={12}
                        marginRight={3}
                    >
                        <BellRing size={22} color="white" />
                    </Box>
                    <Box className={BannerStyles['title']}>
                        Big news! We&apos;re excited to announce a brand new
                        product.
                    </Box>
                </Flex>
                <Flex alignItems="center">
                    <Button
                        marginRight={3}
                        className={`${BannerStyles['button']} ${BannerStyles['button-right']}`}
                    >
                        Learn more
                    </Button>
                    <Button
                        background="transparent"
                        _active={{ outline: '2px solid white' }}
                        _hover={{ background: 'transparent' }}
                        color="#4f46e5"
                    >
                        <X color="white" />
                    </Button>
                </Flex>
            </Flex>
            <Button
                className={`${BannerStyles['button']} ${BannerStyles['button-large']}`}
                w="100%"
            >
                Learn more
            </Button>
        </Box>
    )
}

export default Banner
