import { Button } from '@chakra-ui/button'
import { Menu, MenuButton, MenuList, MenuItem } from '@chakra-ui/menu'
import React from 'react'
import { ChevronDown } from 'lucide-react'
import { MenuButtonProps, Flex, Text, Image } from '@chakra-ui/react'

interface Option {
    value: string
    key: string
    icon?: string
}

interface Props {
    options: Option[]
    value: string
    onChange?: (option: Option) => any
    placeholder?: string
    buttonStyles?: MenuButtonProps
}

const Select: React.FC<Props> = (props) => {
    const current = props.options.find((option) => option.key === props.value)

    return (
        <Menu closeOnBlur matchWidth>
            <MenuButton
                bgColor="transparent"
                _hover={{
                    bg: 'transparent',
                }}
                _active={{
                    bg: 'transparent',
                }}
                as={Button}
                rightIcon={<ChevronDown />}
                textAlign="left"
                fontWeight={400}
                {...props.buttonStyles}
            >
                {current?.icon ? (
                    <Flex alignItems="center">
                        {current.icon && (
                            <Image
                                height="25px"
                                marginRight="5px"
                                src={current.icon}
                            />
                        )}
                        <Text color={props.buttonStyles?.color}>
                            {current.value}
                        </Text>
                    </Flex>
                ) : (
                    props.placeholder || props.value
                )}
            </MenuButton>
            <MenuList w="100%" bg={props.buttonStyles?.bg || 'white'}>
                {props.options.map((option) => (
                    <MenuItem
                        bg="transparent"
                        color={props.buttonStyles?.color}
                        key={option.key}
                        fontWeight={400}
                        _hover={{
                            bg: 'transparent',
                        }}
                        _active={{
                            bg: 'transparent',
                        }}
                        _focus={{
                            bg: 'transparent',
                        }}
                        onClick={() => props.onChange?.(option)}
                    >
                        <Flex alignItems="center">
                            {option.icon && (
                                <Image
                                    height="25px"
                                    marginRight="5px"
                                    src={option.icon}
                                />
                            )}
                            <Text>{option.value}</Text>
                        </Flex>
                    </MenuItem>
                ))}
            </MenuList>
        </Menu>
    )
}

export default Select
