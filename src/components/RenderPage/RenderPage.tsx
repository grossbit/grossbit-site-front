import { GridItem, Grid } from '@chakra-ui/react'
import React from 'react'
import {
    GridElement,
    ListElement,
    ArrayElements,
} from '../../types/Elements.type'
import RenderPageStyles from './RenderPage.module.scss'

interface Props {
    elements: ArrayElements
}

const GridLayout: React.FC<GridElement> = (props) => {
    return (
        <Grid
            gap={4}
            templateRows="repeat(2, 1fr)"
            templateColumns="repeat(10, 1fr)"
            className={RenderPageStyles['grid-layout']}
        >
            {props.content.map((gridItem, idx) => {
                const colSpan = gridItem.size === 'large' ? 10 : 5

                switch (gridItem.type) {
                    case 'image':
                        return (
                            <GridItem
                                key={idx}
                                colSpan={colSpan}
                                rowSpan={30}
                                bgImage={gridItem.src}
                                bgSize="cover"
                                bgRepeat="no-repeat"
                                bgPosition="center"
                            />
                        )
                    case 'text':
                        return (
                            <GridItem
                                key={idx}
                                colSpan={colSpan}
                                rowSpan={15}
                                className={RenderPageStyles['text']}
                            >
                                <p>{gridItem.text}</p>
                            </GridItem>
                        )
                }
            })}
        </Grid>
    )
}

const ListLayout: React.FC<ListElement> = (props) => {
    return (
        <ul>
            {props.items.map((item) => (
                <li key={item}>{item}</li>
            ))}
        </ul>
    )
}

const RenderPage: React.FC<Props> = (props) => {
    return (
        <>
            {props.elements.map((element, idx) => {
                switch (element.type) {
                    case 'text':
                        return <p key={idx}>{element.text}</p>
                    case 'heading':
                        return <h2 key={idx}>{element.text}</h2>
                    case 'image':
                        return <img key={idx} src={element.src} />
                    case 'grid':
                        return <GridLayout {...element} />
                    case 'list':
                        return <ListLayout {...element} />
                }
            })}
        </>
    )
}

export default RenderPage
