import { Box, Text, Flex, Button } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import CookieNotificationStyles from './CookieNotification.module.scss'

const CookieNotification: React.FC = () => {
    const [isOpen, setOpen] = useState(false)
    const router = useRouter()

    useEffect(() => {
        const status = localStorage.getItem('@studio/cookie-state')
        setOpen(status === null)
    }, [router.pathname])

    function onResolve() {
        localStorage.setItem('@studio/cookie-state', 'accepted')
        setOpen(false)
    }

    useEffect(() => {})

    return (
        <Flex
            className={CookieNotificationStyles['container']}
            display={isOpen ? 'flex' : 'none'}
        >
            <Box color="white">
                <Text>Данный сайт использует cookie</Text>
                <Text fontSize="sm" textDecoration="underline">
                    <a
                        target="_blank"
                        rel="noreferrer"
                        href="https://developer.mozilla.org/ru/docs/Web/HTTP/Cookies"
                    >
                        Что такое файлы-cookie?
                    </a>
                </Text>
            </Box>
            <Button onClick={onResolve}>Окей</Button>
        </Flex>
    )
}

export default CookieNotification
