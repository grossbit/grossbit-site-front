import React, { useEffect, useState } from 'react'
import {
    Box,
    Button,
    Flex,
    Spacer,
    useColorMode,
    Tooltip,
    Image,
    useDisclosure,
    Drawer,
    DrawerBody,
    DrawerCloseButton,
    DrawerContent,
    DrawerHeader,
    DrawerOverlay,
} from '@chakra-ui/react'
import { Sun, Moon, Menu } from 'lucide-react'
import HeaderStyles from './Header.module.scss'
import { useRouter } from 'next/router'
import NextLink from 'next/link'
import { WithTranslation, withTranslation } from 'react-i18next'
import LanguageSelector from '../../LanguageSelector/LanguageSelector'
import { HeaderCustomization } from './Header.props'

interface Props extends WithTranslation {
    link?: string
    styles?: HeaderCustomization
}

const links = [
    {
        name: 'main',
        path: '/',
    },
    {
        name: 'portfolio',
        path: '/portfolio',
    },
    {
        name: 'services',
        path: '/services',
    },
    {
        name: 'careers',
        path: '/careers',
    },
    {
        name: 'about',
        path: '/about',
    },
]

const Header: React.FC<Props> = (props) => {
    const { colorMode, toggleColorMode } = useColorMode()
    const { pathname } = useRouter()
    const { isOpen, onClose, onOpen } = useDisclosure()
    const [isScrolled, setScrolled] = useState(false)

    useEffect(() => {
        function scrollHandler() {
            setScrolled(
                document.body.scrollTop > 50 ||
                    document.documentElement.scrollTop > 50
            )
        }

        document.addEventListener('scroll', scrollHandler)
        return () => document.removeEventListener('scroll', scrollHandler)
    }, [])

    const bgColor =
        (isScrolled
            ? props.styles?.scrollBgColor || props.styles?.bgColor
            : props.styles?.bgColor) || 'white'

    const fontColor =
        (isScrolled
            ? props.styles?.scrollFontColor || props.styles?.fontColor
            : props.styles?.fontColor) || 'silver'

    const activeColor =
        (isScrolled
            ? props.styles?.scrollActiveFontColor ||
              props.styles?.activeFontColor
            : props.styles?.activeFontColor) || 'black'

    function getLinkStyles(path: string) {
        const isActive = pathname === path || props.link === path
        return {
            color: isActive ? activeColor : fontColor,
        }
    }

    return (
        <Box position="sticky" top={-1} w="100%" zIndex={999}>
            {/* <Banner /> */}
            <Box
                className={HeaderStyles['header']}
                alignItems="center"
                bg={bgColor}
                h={isScrolled ? 'auto' : '80px'}
            >
                <Flex alignItems="center">
                    <Box display={isScrolled ? 'none' : 'block'} width="200px">
                        <Flex>
                            <Image
                                src={'/assets/logo/dark.png'}
                                alt={'logo'}
                                objectFit="contain"
                                width="150px"
                                height="60px"
                            />
                        </Flex>
                    </Box>
                    <Spacer display={isScrolled ? 'none' : 'block'} />
                    <Flex className={HeaderStyles['header-nav__links']}>
                        {links.map((link) => {
                            return (
                                <Box
                                    key={link.path}
                                    style={{
                                        ...getLinkStyles(link.path),
                                        padding: '5px 10px',
                                        borderRadius: '5px',
                                    }}
                                >
                                    <NextLink
                                        key={link.path}
                                        href={link.path}
                                        as={link.path}
                                    >
                                        {props.t(`navigation.${link.name}`)}
                                    </NextLink>
                                </Box>
                            )
                        })}
                    </Flex>
                    <Spacer />
                    <Box paddingRight="2%">
                        <Flex className={HeaderStyles['options']}>
                            <Box marginRight="10%">
                                <LanguageSelector
                                    color={activeColor}
                                    bg={props.styles?.bgColor}
                                />
                            </Box>
                            <Tooltip label={props.t('tooltip.toggle_theme')}>
                                <Button
                                    display="none"
                                    onClick={toggleColorMode}
                                    padding="7.5px 5px"
                                    variant={colorMode}
                                    borderRadius="100%"
                                >
                                    {colorMode === 'light' ? <Moon /> : <Sun />}
                                </Button>
                            </Tooltip>
                        </Flex>
                        <Box className={HeaderStyles['drawer']}>
                            <Button onClick={onOpen}>
                                <Menu />
                            </Button>
                            <Drawer
                                isFullHeight
                                isOpen={isOpen}
                                placement="right"
                                onClose={onClose}
                            >
                                <DrawerOverlay />
                                <DrawerContent bgColor={bgColor}>
                                    <DrawerCloseButton />
                                    <DrawerHeader color={activeColor}>
                                        Lorem ipsum
                                    </DrawerHeader>
                                    <DrawerBody>
                                        {links.map((link) => {
                                            return (
                                                <Box
                                                    key={link.path}
                                                    style={{
                                                        padding: '10px',
                                                        ...getLinkStyles(
                                                            link.path
                                                        ),
                                                    }}
                                                >
                                                    <NextLink
                                                        key={link.path}
                                                        href={link.path}
                                                        as={link.path}
                                                    >
                                                        {props.t(
                                                            `navigation.${link.name}`
                                                        )}
                                                    </NextLink>
                                                </Box>
                                            )
                                        })}
                                        <Flex
                                            alignItems="center"
                                            justifyContent="space-between"
                                            paddingTop="10px"
                                        >
                                            <Box>
                                                <LanguageSelector
                                                    color={activeColor}
                                                />
                                            </Box>
                                            <Tooltip
                                                label={props.t(
                                                    'tooltip.toggle_theme'
                                                )}
                                            >
                                                <Button
                                                    display="none"
                                                    onClick={toggleColorMode}
                                                    padding="7.5px 5px"
                                                >
                                                    {colorMode === 'light' ? (
                                                        <Moon />
                                                    ) : (
                                                        <Sun />
                                                    )}
                                                </Button>
                                            </Tooltip>
                                        </Flex>
                                    </DrawerBody>
                                </DrawerContent>
                            </Drawer>
                        </Box>
                    </Box>
                </Flex>
            </Box>
        </Box>
    )
}

export default withTranslation()(Header)
