interface HeaderCustomization {
    bgColor?: string
    fontColor?: string
    activeFontColor?: string
    scrollBgColor?: string
    scrollFontColor?: string
    scrollActiveFontColor?: string
}

export type { HeaderCustomization }
