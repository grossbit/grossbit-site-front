import React from 'react'
import Header from './Header/Header'
import SEO from '../SEO/SEO'
import { Avatar, Text, Box, Flex } from '@chakra-ui/react'
import Link from 'next/link'
import CookieNotification from '../CookieNotification/CookieNotification'
import LayoutStyles from './Layout.module.scss'
import { HeaderCustomization } from './Header/Header.props'
interface Props {
    title?: string
    link?: string
    header?: HeaderCustomization
    container?: {
        bg?: string
        fontColor?: string
    }
    pythia?: {
        bgColor?: string
        fontColor?: string
    }
}

const Layout: React.FC<Props> = (props) => {
    return (
        <Box>
            <CookieNotification />
            <SEO title={props.title || 'Lorem Ipsum'} />
            <Box
                className={LayoutStyles['container']}
                color={props.container?.fontColor || 'black'}
                bg={props.container?.bg || 'white'}
            >
                <Header styles={props.header} link={props.link} />
                {props.children}
            </Box>

            <Link href="/chat" passHref>
                <Flex
                    position="fixed"
                    zIndex={Infinity}
                    bottom="10px"
                    right="10px"
                    maxW="300px"
                    background={props.pythia?.bgColor || 'white'}
                    padding={2}
                    alignItems="center"
                    borderRadius={40}
                    cursor="pointer"
                    paddingX={3}
                >
                    <Avatar src="/assets/bot-avatar.gif" marginRight={2} />
                    <Text
                        color={props.pythia?.fontColor || 'black'}
                        fontSize="xl"
                    >
                        Пообщаемся?
                    </Text>
                </Flex>
            </Link>
        </Box>
    )
}

export default Layout
