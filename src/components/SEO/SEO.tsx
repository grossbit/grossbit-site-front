import React from 'react'
import NextHead from 'next/head'

interface Props {
    title?: string
}

const SEO: React.FC<Props> = (props) => {
    return (
        <>
            <NextHead>
                <meta charSet="utf-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta
                    name="viewport"
                    content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
                />
                <meta name="description" content="Description" />
                <title>
                    {props.title
                        ? `${props.title} - Lorem Ipsum`
                        : 'Lorem Ipsum'}
                </title>
                <link rel="manifest" href="/manifest.json" />
            </NextHead>
            {props.children}
        </>
    )
}

export default SEO
