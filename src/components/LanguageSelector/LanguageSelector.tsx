import React from 'react'
import { useTranslation } from 'react-i18next'
import parseLang from '../../lib/parseLang'
import i18next from 'i18next'
import Select from '../Select/Select'

const languages = ['ru', 'en']

interface Props {
    color?: string
    bg?: string
}

const LanguageSelector: React.FC<Props> = (props) => {
    const { t } = useTranslation()
    return (
        <Select
            options={languages.map((lang) => ({
                key: lang,
                value: t(`languages.${lang}`),
            }))}
            buttonStyles={{ color: props.color, bg: props.bg }}
            value={t(`languages.${parseLang()}`)}
            onChange={(option) => i18next.changeLanguage(option.key)}
        />
    )
}

export default LanguageSelector
