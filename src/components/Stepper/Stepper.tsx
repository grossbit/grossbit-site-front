import React from 'react'
import { Box, Flex, Grid, GridItem, Text } from '@chakra-ui/react'
import { Check, Circle } from 'lucide-react'
import StepperStyles from './Stepper.module.scss'

const steps = [
    {
        title: 'Setup environment',
        completed: true,
        subtitle: 'Fill a form',
    },
    {
        title: 'Install dependencies',
        completed: false,
        subtitle: 'Modules',
    },
    {
        title: 'Run an application',
        completed: false,
        subtitle: 'npm run dev',
    },
]

const Stepper: React.FC = () => {
    return (
        <>
            <Box w="100%" className={StepperStyles['small-stepper']}>
                {steps.map((step, index) => {
                    const isNeedOutline = steps[index - 1]?.completed
                    return (
                        <Box key={step.title}>
                            {index !== 0 && (
                                <Box
                                    w="3px"
                                    h="30px"
                                    background={
                                        isNeedOutline ? '#4f46e5' : 'silver'
                                    }
                                    marginLeft="20px"
                                />
                            )}
                            <Flex alignItems="center">
                                <Flex
                                    alignItems="center"
                                    justifyContent="center"
                                    background={
                                        step.completed ? '#4f46e5' : 'white'
                                    }
                                    border={
                                        !step.completed
                                            ? `2px solid ${
                                                  isNeedOutline
                                                      ? '#4f46e5'
                                                      : 'silver'
                                              }`
                                            : 'none'
                                    }
                                    padding={2}
                                    w={45}
                                    h={45}
                                    minW={45}
                                    minH={45}
                                    borderRadius="100%"
                                >
                                    {step.completed ? (
                                        <Check color="white" size={25} />
                                    ) : isNeedOutline ? (
                                        <Circle
                                            fill="#4f46e5"
                                            color="#4f46e5"
                                            size={12.5}
                                        />
                                    ) : null}
                                </Flex>
                                <Box marginLeft="10px">
                                    <Text
                                        textTransform="uppercase"
                                        fontSize="md"
                                    >
                                        {step.title}
                                    </Text>
                                    <Text color="silver" fontSize="sm">
                                        {step.subtitle}
                                    </Text>
                                </Box>
                            </Flex>
                        </Box>
                    )
                })}
            </Box>

            <Grid
                templateColumns="repeat(3, 1fr)"
                className={StepperStyles['large-stepper']}
            >
                {steps.map((step, id) => (
                    <GridItem colSpan={1} key={step.title}>
                        <Flex alignItems="center" h="100%">
                            <Flex
                                alignItems="center"
                                w={45}
                                h={45}
                                minH={45}
                                minW={45}
                                justifyContent="center"
                                background={
                                    step.completed ? '#4f46e5' : 'white'
                                }
                                color={step.completed ? 'white' : '#4f46e5'}
                                border="2px solid #4f46e5"
                                padding={2}
                                borderRadius="100%"
                            >
                                {step.completed ? (
                                    <Check size={25} color="white" />
                                ) : (
                                    `0${id + 1}`
                                )}
                            </Flex>
                            <Box marginLeft="15px">
                                <Text fontSize="md" textTransform="uppercase">
                                    {step.title}
                                </Text>
                                <Text fontSize="sm" color="#6B7280">
                                    {step.subtitle}
                                </Text>
                            </Box>
                        </Flex>
                    </GridItem>
                ))}
            </Grid>
        </>
    )
}

export default Stepper
