import { Box, Text, Flex } from '@chakra-ui/react'
import React from 'react'
import LogoCloudsStyles from './LogoClouds.module.scss'

const LogoClouds: React.FC = () => {
    return (
        <Box padding={5} borderRadius={10} background="#4338ca">
            <Text color="white" textAlign="center" fontSize={45}>
                Trusted by
            </Text>
            <Flex
                className={LogoCloudsStyles['clouds']}
                justifyContent="center"
                flexWrap="wrap"
            >
                <img src="https://d33wubrfki0l68.cloudfront.net/270b9f04f573d5a5bf2fe1e20c52e09b85f2398e/3dbc6/red-hat-logo.c5e6e64a.svg" />
                <img src="https://d33wubrfki0l68.cloudfront.net/554b0594b9de16144c7d6117c61e00d2012d61f5/a3eea/trilon.1777374a.svg" />
                <img src="https://d33wubrfki0l68.cloudfront.net/52e307e6174d50e1642f8132dc5e46cc32ffb822/d2b33/casinoonline-logo.8bae5f5a.png" />
                <img src="https://d33wubrfki0l68.cloudfront.net/6aaa13e1a651dc22bef587c69c48b0040d195eba/85bb3/angularmix-logo.362868a5.png" />
            </Flex>
        </Box>
    )
}

export default LogoClouds
