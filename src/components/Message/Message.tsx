import React from 'react'
import {
    FileMessageData,
    Message,
    MessageAuthor,
    TextMessageData,
} from '../../types/Message.type'
import { isLink } from '../../lib/validation'
import {
    Box,
    Flex,
    Grid,
    GridItem,
    Spinner,
    Text,
    Image as ChakraImage,
} from '@chakra-ui/react'
import MessageStyles from './Message.module.scss'
import formatDate from '../../lib/formatDate'
import { Music, Video, Image, File } from 'lucide-react'

type Props = {
    message: Message
    loading?: boolean
    onCallback?: (callback: string, messageId: string, text: string) => any
}

const Message: React.FC<Props> = ({ message, loading, onCallback }) => {
    const isBot = message.author === MessageAuthor.BOT
    const isText = message.data_type !== 'file'
    const date = formatDate(message.date)

    function getMessageContent(content: string) {
        const nodes = ` ${content}`.split(' ')
        return nodes.map((node) => {
            if (isLink(node)) {
                return (
                    <a
                        target="_blank"
                        rel="noreferrer"
                        className="text-blue-500"
                        href={node}
                    >
                        {node}{' '}
                    </a>
                )
            }
            return `${node} `
        })
    }

    function getFileCover() {
        const styles = { color: 'white', size: 32 }
        const mediaUnit = (message.data as FileMessageData).media_unit
        const mediaUrl = (message.data as FileMessageData).media_url

        if (loading) {
            return {
                icon: <Spinner size="md" color="white" />,
                preview: null,
            }
        }

        if (mediaUnit.includes('image'))
            return {
                icon: <Image {...styles} />,
                preview: (
                    <ChakraImage
                        src={mediaUrl}
                        position="relative"
                        boxSize="100%"
                        objectFit="cover"
                        borderRadius={10}
                        alt={mediaUrl}
                    />
                ),
            }
        if (mediaUnit.includes('video'))
            return {
                icon: <Video {...styles} />,
                preview: <video controls src={mediaUrl} />,
            }
        if (mediaUnit.includes('audio'))
            return {
                icon: <Music {...styles} />,
                preview: (
                    <audio style={{ width: '100%' }} src={mediaUrl} controls />
                ),
            }

        return {
            icon: <File {...styles} />,
            preview: null,
        }
    }

    function getFilename() {
        const filename = (message.data as FileMessageData).filename
        const mediaUrl = (message.data as FileMessageData).media_url
        if (filename) return filename
        return new URL(mediaUrl).searchParams.get('filename')
    }
    return (
        <Flex justifyContent={isBot ? 'flex-start' : 'flex-end'}>
            <Box
                backgroundColor={isBot ? ' #f2f3f4 ' : '#5099f4'}
                color={isBot ? 'black' : 'white'}
                className={MessageStyles['content']}
            >
                {isText ? (
                    <>
                        <span>
                            {getMessageContent(
                                (message.data as TextMessageData).content
                            )}
                        </span>
                        <span className={MessageStyles['date']}>
                            {`${date.hours}:${date.minutes}`}
                        </span>
                        {!!message.buttons && (
                            <Grid
                                marginTop={2}
                                w="100%"
                                templateRows="repeat(2, 1fr)"
                                templateColumns="repeat(4, 1fr)"
                            >
                                {message.buttons.map((button) => (
                                    <GridItem
                                        key={button.callback}
                                        position="relative"
                                        cursor="pointer"
                                        bgColor="rgba(255, 255, 255, 0.75)"
                                        textAlign="center"
                                        borderRadius="5px"
                                        color="black"
                                        rowSpan={1}
                                        colSpan={4}
                                        marginBottom={1}
                                        padding={2}
                                        fontSize={14}
                                        onClick={() =>
                                            onCallback?.(
                                                button.callback,
                                                message.id,
                                                button.text
                                            )
                                        }
                                    >
                                        {button.text}
                                    </GridItem>
                                ))}
                            </Grid>
                        )}
                    </>
                ) : (
                    <a
                        href={(message.data as FileMessageData).media_url}
                        target="_blank"
                        rel="noreferrer"
                    >
                        {message.author !== MessageAuthor.BOT && (
                            <Flex
                                alignItems="center"
                                paddingX="10px"
                                cursor="pointer"
                            >
                                <Box
                                    marginRight="10px"
                                    background="#4b91f1"
                                    padding="10px"
                                    borderRadius={9999}
                                >
                                    {getFileCover().icon}
                                </Box>
                                <Box>
                                    <Text fontSize={15}>{getFilename()}</Text>
                                    <Text>
                                        {(message.data as FileMessageData).size}
                                    </Text>
                                </Box>
                            </Flex>
                        )}
                        {(message.data as FileMessageData).media_url && (
                            <Box marginTop={3}>{getFileCover().preview}</Box>
                        )}
                    </a>
                )}
            </Box>
        </Flex>
    )
}

export default Message
