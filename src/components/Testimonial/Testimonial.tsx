import { Box, Grid, GridItem, Text, Flex, Avatar } from '@chakra-ui/react'
import React from 'react'
import TestimonialStyles from './Testimonial.module.scss'
import { Quote } from 'lucide-react'

const Testimonial: React.FC = () => {
    return (
        <Grid
            templateColumns="repeat(2, 1fr)"
            w="100%"
            className={TestimonialStyles['container']}
        >
            <GridItem colSpan={1}>
                <Text fontSize={'3xl'}>Tuple</Text>
                <Box minH="130px">
                    <Quote size={24} color="#4f46e5" fill="#4f46e5" />
                    <Box>
                        You can’t connect the dots looking forward; you can only
                        connect them looking backwards. So you have to trust
                        that the dots will somehow connect in your future
                    </Box>
                </Box>
                <Flex>
                    <Avatar
                        className={TestimonialStyles['avatar']}
                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoE-0Y8wz0PyX3awnPEsJSkC2K47ytO6jjVX2yD-_8_S-bF8AR4O6QKQr_vCWNCCmrJ5s&usqp=CAU"
                    />
                    <Box marginLeft={3}>
                        <Box fontSize={16}>Steve Jobs</Box>
                        <Box fontSize={16} color="#c7d2fe">
                            Founder Apple
                        </Box>
                    </Box>
                </Flex>
            </GridItem>
            <GridItem colSpan={1}>
                <Text fontSize={'3xl'}>Tuple</Text>
                <Box>
                    <Quote size={24} color="#4f46e5" fill="#4f46e5" />
                    <Box>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Quod impedit illo quas eius officiis, quidem nemo magnam
                        voluptatum reprehenderit doloremque atque reiciendis.
                        Saepe repellat mollitia natus deleniti cum? Ipsam, a.
                    </Box>
                </Box>
                <Flex>
                    <Avatar
                        name="Dan Abramov"
                        className={TestimonialStyles['avatar']}
                        src="https://bit.ly/dan-abramov"
                    />
                    <Box marginLeft={3}>
                        <Box fontSize={16}>Dan Abramov</Box>
                        <Box fontSize={16} color="#c7d2fe">
                            React.js developer
                        </Box>
                    </Box>
                </Flex>
            </GridItem>
        </Grid>
    )
}

export default Testimonial
