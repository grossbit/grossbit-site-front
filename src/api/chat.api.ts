import { Message } from '../types/Message.type'
import { axios } from './axios'

enum Methods {
    INIT_CONVERSATION = 'initConvo',
    NEW_MESSAGE = 'newMessage',
}

type MessageDto = Omit<Message, 'date' | 'id' | 'buttons'>

type ResponseMessages = {
    conversation_id: string
    messages: Message[]
}

export const getConversationMessages = async (
    c_id: string
): Promise<ResponseMessages> => {
    const { data } = await axios.get('/pythia', {
        params: {
            conversation_id: c_id,
        },
    })
    return data
}

type StartConversationResponse = { conversation_id: string } & (
    | ResponseMessages
    | { total_messages: number }
)

export const startConversation = async (
    c_id?: string | null
): Promise<StartConversationResponse> => {
    const { data } = await axios.post('/pythia', {
        method: Methods.INIT_CONVERSATION,
        conversation_id: c_id,
    })
    return data
}

export const sendMessage = async ({
    messages,
    conversation_id,
}: {
    messages: MessageDto[]
    conversation_id: string
}): Promise<ResponseMessages> => {
    const { data } = await axios.post('/pythia', {
        method: Methods.NEW_MESSAGE,
        conversation_id,
        messages,
    })
    return data
}
