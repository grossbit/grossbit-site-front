import CareerCard from '../types/CareerCard.type'
import PageData from '../types/PageData'
import { axios } from './axios'

export const getCareers = async (): Promise<Array<[string, CareerCard[]]>> => {
    try {
        const { data } = await axios.get('/vacancy_containers')
        return Object.entries(data)
    } catch (e) {
        console.log(e)
        return []
    }
}

export const getCareer = async (_id: string): Promise<PageData> => {
    const { data } = await axios.get('/vacancy', {
        params: { _id },
    })
    return data
}

export const createOffer = async (offer: {
    vacancy_id: string
    firstname: string
    lastname: string
    email: string
    phone: string
    message: string
    resume_url: string
}) => {
    const { data } = await axios.post('/offer', offer)
    return data
}

export const getCareersList = async (): Promise<{ id: string }[]> => {
    const { data } = await axios.get('/vacancies_list')
    return data
}
