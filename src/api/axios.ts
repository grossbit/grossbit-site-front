import axs from 'axios'
import { apiBaseURL } from '../constants/env'

export const axios = axs.create({
    baseURL: `${apiBaseURL}/api/v1/`,
})
