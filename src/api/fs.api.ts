import { axios } from './axios'

export const uploadFile = async (
    file: File
): Promise<{ file_url: string | null }> => {
    try {
        const formData = new FormData()
        formData.append('file', file)
        const { data } = await axios.post('/fs', formData)
        return data
    } catch (e) {
        return { file_url: null }
    }
}
