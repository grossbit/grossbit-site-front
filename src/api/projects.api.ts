import PageData from '../types/PageData'
import ProjectCard from '../types/ProjectCard.type'
import { axios } from './axios'

export const getProjects = async (params?: {
    [key: string]: any
}): Promise<{
    nextPage: boolean
    data: ProjectCard[]
}> => {
    try {
        const { data } = await axios.get('/case_containers', { params })
        return data
    } catch (e) {
        return { nextPage: false, data: [] }
    }
}

export const getProject = async (_id: string): Promise<PageData> => {
    const { data } = await axios.get('/case', { params: { _id } })
    return data
}

export const getProjectsList = async (): Promise<{ id: string }[]> => {
    const { data } = await axios.get('/detail_cases_list')
    return data
}
