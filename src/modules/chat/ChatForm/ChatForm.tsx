import { Flex, Checkbox, Input } from '@chakra-ui/react'
import filesize from 'filesize'
import { Paperclip, Send } from 'lucide-react'
import React, { ChangeEventHandler, useRef, useState } from 'react'
import { sendMessage } from '../../../api/chat.api'
import { Message, MessageAuthor } from '../../../types/Message.type'
import styles from './ChatForm.module.scss'
import shortid from 'shortid'
import { uploadFile } from '../../../api/fs.api'

interface ChatFormProps {
    onMessage: (message: Message | Message[]) => any
    openErrorFileModal?: () => any
    conversationId: string
}

const spamProtectionInitial = {
    count: 0,
    attempts: 0,
}

const ONE_SECOND = 1000

export const ChatForm: React.FC<ChatFormProps> = (props) => {
    const fileInputRef = useRef<HTMLInputElement | null>(null)
    const [spamProtection, setSpamProtection] = useState(spamProtectionInitial)

    const [isInvalid, setInvalid] = useState(false)
    const [disabled, setDisabled] = useState(false)
    const [message, setMessage] = useState('')
    const [lastMessageTime, setLastMessageTime] = useState(0)

    const addMessage = async () => {
        if (!message.trim().length) return setInvalid(true)

        const now = Date.now()
        const difference = now - lastMessageTime

        if (ONE_SECOND > difference) {
            setSpamProtection(({ count, attempts }) => {
                if (count >= 5) {
                    setDisabled(true)
                    setInvalid(true)
                    setTimeout(() => {
                        setDisabled(false)
                        setInvalid(false)
                    }, 2000)
                    return { count: 0, attempts: attempts + 1 }
                }
                return { count: count + 1, attempts }
            })
        }

        const wordsPerMessage = 150
        const words = message.replaceAll(/\s+/g, ' ').trim().split(' ')
        const messagesCount = Math.ceil(words.length / wordsPerMessage)

        const messages: Message[] = Array.from({ length: messagesCount }).map(
            (e, i) => ({
                id: shortid.generate(),
                date: new Date(),
                author: MessageAuthor.USER,
                data_type: 'text',
                data: {
                    content: words
                        .slice(i * wordsPerMessage, (i + 1) * wordsPerMessage)
                        .join(' '),
                },
            })
        )
        props.onMessage(messages)
        const data = await sendMessage({
            messages,
            conversation_id: props.conversationId as string,
        })
        props.onMessage(data.messages)
        setInvalid(false)
        setMessage('')
        setLastMessageTime(now)
    }

    const enterHandler = (e: any) => {
        if (e.key === 'Enter') {
            addMessage()
        }
    }

    const handleFiles: ChangeEventHandler<HTMLInputElement> = async (e) => {
        const files = (e.target.files || []) as File[]
        let size: number = 0
        for (const file of files) {
            size += file.size
        }

        if (parseInt((size / 1024 / 1024).toFixed(4)) > 100) {
            return props.openErrorFileModal?.()
        }

        for (const file of files) {
            const id = shortid.generate()
            const fileMessage = {
                id,
                data_type: 'file',
                author: MessageAuthor.USER,
                date: new Date(),
                data: {
                    media_url: URL.createObjectURL(file),
                    media_unit: file.type,
                    filename: file.name,
                    size: filesize(file.size),
                },
            }
            props.onMessage(fileMessage as Message)
            const data = await uploadFile(file)
            const { messages } = await sendMessage({
                conversation_id: props.conversationId,
                messages: [
                    {
                        ...fileMessage,
                        data: { ...fileMessage.data, media_url: data.file_url },
                    } as any,
                ],
            })
            props.onMessage(messages)
        }
    }

    const resolveCaptcha = () => {
        setSpamProtection(spamProtectionInitial)
        setDisabled(false)
        setInvalid(false)
    }

    return (
        <Flex className={styles.form}>
            {spamProtection.attempts >= 3 ? (
                <Checkbox
                    size="lg"
                    onChange={resolveCaptcha}
                    colorScheme="green"
                >
                    Подтвердите, что вы не робот!
                </Checkbox>
            ) : (
                <>
                    <Flex
                        minH="45px"
                        h="45px"
                        minW="45px"
                        w="45px"
                        alignItems="center"
                        justifyContent="center"
                        borderRadius="500px"
                        background="white"
                        cursor="pointer"
                        paddingTop="1px"
                        onClick={() => fileInputRef.current?.click()}
                    >
                        <input
                            type="file"
                            onChange={handleFiles}
                            multiple
                            max={10}
                            maxLength={10}
                            style={{ display: 'none' }}
                            accept=".docx, .mp4, .mp3, .csv, .doc, .txt, .png, .jpg, .jpeg, .webp"
                            ref={fileInputRef}
                        />
                        <Paperclip size={24} color="#438bfa" />
                    </Flex>
                    <Input
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                        onKeyUp={enterHandler}
                        isInvalid={isInvalid}
                        size="lg"
                        disabled={disabled}
                        borderRadius={0}
                        focusBorderColor="transparent"
                        placeholder="Сообщение"
                        border="none"
                        className={styles.input}
                    />
                    <Flex
                        minH="45px"
                        h="45px"
                        minW="45px"
                        w="45px"
                        alignItems="center"
                        justifyContent="center"
                        onClick={addMessage}
                        cursor="pointer"
                        paddingRight="4px"
                        paddingTop="1px"
                    >
                        <Send size={24} color="#438bfa" />
                    </Flex>
                </>
            )}
        </Flex>
    )
}
