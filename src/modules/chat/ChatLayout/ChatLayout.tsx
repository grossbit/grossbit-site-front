import { Box } from '@chakra-ui/react'
import React from 'react'

export const ChatLayout: React.FC = ({ children }) => {
    return (
        <Box
            position="fixed"
            top={0}
            bottom={0}
            left={0}
            right={0}
            minW="380px"
            maxH="100%"
            minH={0}
        >
            {children}
        </Box>
    )
}
