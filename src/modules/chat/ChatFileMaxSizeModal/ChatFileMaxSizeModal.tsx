import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
    Button,
} from '@chakra-ui/react'
import React from 'react'

interface ChatFileMaxSizeModalProps {
    isOpen: boolean
    onClose: () => any
    maxSize?: string
}

export const ChatFileMaxSizeModal: React.FC<ChatFileMaxSizeModalProps> = (
    props
) => {
    return (
        <Modal isOpen={props.isOpen} onClose={props.onClose} isCentered>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Превышен размер загруженных файлов</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    Вы загрузили файлы, которые весят больше чем доступно (
                    {props.maxSize || '100мб'})
                </ModalBody>
                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={props.onClose}>
                        Закрыть
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}
