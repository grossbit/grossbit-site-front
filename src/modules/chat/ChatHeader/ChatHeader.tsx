import React from 'react'
import { useRouter } from 'next/router'
import { Flex, Text, Box, Avatar, Tooltip } from '@chakra-ui/react'
import { ChevronLeft } from 'lucide-react'
import styles from './ChatHeader.module.scss'

export const ChatHeader: React.FC = () => {
    const history = useRouter()

    function backPage() {
        if (window.history.state.idx) {
            history.back()
        } else {
            history.replace('/')
        }
    }

    return (
        <Flex className={styles.header}>
            <Flex alignItems={'center'}>
                <Box cursor="pointer" marginRight="10%">
                    <ChevronLeft onClick={backPage} color="black" size={34} />
                </Box>
                <Avatar marginRight={2} src="/assets/bot-avatar.gif" />
                <Box>
                    <Flex alignItems={'center'}>
                        <Text fontWeight={600} fontSize={18}>
                            Пифия
                        </Text>
                        <Tooltip label="online">
                            <Box
                                borderRadius={50}
                                width={2.5}
                                height={2.5}
                                marginLeft={'5px'}
                                bgColor={'#1ABC9C'}
                            />
                        </Tooltip>
                    </Flex>
                    <Text
                        marginTop={'-5px'}
                        color="#C0C0C0"
                        fontWeight={500}
                        fontSize={14}
                    >
                        В сети
                    </Text>
                </Box>
            </Flex>
        </Flex>
    )
}
