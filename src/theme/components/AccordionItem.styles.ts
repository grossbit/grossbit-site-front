const accordionStyles = {
    baseStyle: {
        _focus: { boxShadow: 'none' },
        _hover: { background: 'white' },
    },
}

export { accordionStyles }
