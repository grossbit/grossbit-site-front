const buttonStyles = {
    variants: {
        dark: {
            bg: 'gray.800',
            color: 'white',
            borderWidth: '1px',
        },
        light: {
            borderWidth: '1px',
            borderColor: 'gray.800',
            bg: 'white',
            color: 'black',
        },
    },
    baseStyle: { _focus: { boxShadow: 'none' } },
}

export { buttonStyles }
