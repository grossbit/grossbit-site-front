import { mode } from '@chakra-ui/theme-tools'
import { extendTheme, theme as chakraTheme } from '@chakra-ui/react'
import { containerStyles as Container } from './components/Container'
import { buttonStyles as Button } from './components/Button.styles'
import { accordionStyles as AccordionButton } from './components/AccordionItem.styles'

export const theme = extendTheme({
    styles: {
        global: (props: any) => ({
            body: {
                color: mode('black', 'white')(props),
                bg: mode('white', 'dark.900')(props),
                lineHeight: 'base',
            },
        }),
    },
    colors: {
        ...chakraTheme.colors,
        dark: {
            800: '#272727',
            900: '#181713',
        },
    },
    components: {
        Container,
        Button,
        AccordionButton,
    },
})
