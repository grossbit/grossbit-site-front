import { Box, Flex, Text } from '@chakra-ui/react'
import { ChevronRight, Home, LayoutGrid } from 'lucide-react'
import Link from 'next/link'
import React from 'react'
import { useTranslation } from 'react-i18next'
import PageNotFoundStyles from '../styles/404.module.scss'

const routes = [
    {
        path: '/',
        title: 'main',
        icon: Home,
    },
    {
        path: '/portfolio',
        title: 'portfolio',
        icon: LayoutGrid,
    },
]

const PageNotFound: React.FC = () => {
    const { t } = useTranslation()
    return (
        <Flex
            alignItems="center"
            justifyContent="center"
            w="100vw"
            h="100vh"
            paddingX={10}
        >
            <Box>
                <Box textAlign="center">
                    <Text fontSize="lg" color="#4f46e5">
                        {t('404.error')}
                    </Text>
                    <Text fontSize="5xl" fontWeight={700}>
                        {t('404.page_not_exists')}
                    </Text>
                    <Text fontSize="xl" color="silver" fontWeight={700}>
                        {t('404.not_found')}
                    </Text>
                </Box>
                <Box className={PageNotFoundStyles['links']}>
                    {routes.map((route) => (
                        <Box paddingY={5} cursor="pointer" key={route.path}>
                            <Link href={route.path} passHref>
                                <Flex
                                    alignItems="center"
                                    justifyContent="space-between"
                                >
                                    <Flex alignItems="center">
                                        <Flex
                                            alignItems="center"
                                            justifyContent="center"
                                            w={50}
                                            h={50}
                                            borderRadius="10px"
                                            bg="#eef2ff"
                                        >
                                            <route.icon
                                                size={25}
                                                color="#4338ca"
                                            />
                                        </Flex>
                                        <Box marginLeft="15px">
                                            <Text fontSize="20px">
                                                {t(`navigation.${route.title}`)}
                                            </Text>
                                            <Text
                                                fontSize="18px"
                                                color="silver"
                                            >
                                                {t(
                                                    `navigation.${route.title}_slug`
                                                )}
                                            </Text>
                                        </Box>
                                    </Flex>
                                    <ChevronRight color="silver" size={20} />
                                </Flex>
                            </Link>
                        </Box>
                    ))}
                </Box>
                <Link href="/" passHref>
                    <Flex cursor="pointer" alignItems="center" marginTop="20px">
                        <Text color="#4338ca" marginRight="10px">
                            {t('404.back')}
                        </Text>
                        <ChevronRight color="#4338ca" />
                    </Flex>
                </Link>
            </Box>
        </Flex>
    )
}

export default PageNotFound
