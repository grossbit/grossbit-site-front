import { Box, useDisclosure } from '@chakra-ui/react'
import { useState, useRef, useEffect } from 'react'
import OrderPageStyles from '../styles/order-page.module.scss'
import MessageComponent from '../components/Message/Message'
import { ChatLayout } from '../modules/chat/ChatLayout/ChatLayout'
import { ChatFileMaxSizeModal } from '../modules/chat/ChatFileMaxSizeModal/ChatFileMaxSizeModal'
import { ChatForm } from '../modules/chat/ChatForm/ChatForm'
import { Message, MessageAuthor } from '../types/Message.type'
import { ChatHeader } from '../modules/chat/ChatHeader/ChatHeader'
import {
    getConversationMessages,
    sendMessage,
    startConversation,
} from '../api/chat.api'
import storageKeys from '../constants/storageKeys'
import shortid from 'shortid'

const OrderPage = () => {
    const [messages, setMessages] = useState<Message[]>([])
    const messagesRef = useRef<HTMLDivElement | null>(null)
    const { isOpen, onClose } = useDisclosure()
    const [conversationId, setConversationId] = useState<string | null>(null)

    useEffect(() => {
        if (messagesRef.current) {
            messagesRef.current.scrollTo({
                left: 0,
                top: messagesRef.current.scrollHeight,
                behavior: 'smooth',
            })
        }
    }, [messages])

    useEffect(() => {
        startConversation(
            localStorage.getItem(storageKeys.conversationId)
        ).then((data) => {
            localStorage.setItem(
                storageKeys.conversationId,
                data.conversation_id
            )
            setConversationId(data.conversation_id)
            if ('total_messages' in data) {
                getConversationMessages(data.conversation_id).then(
                    ({ messages }) => {
                        setMessages(messages)
                    }
                )
            } else {
                setMessages(data.messages)
            }
        })
    }, [])

    function onMessage(message: Message[] | Message) {
        setMessages((messages) => {
            const oldMessages = messages.map((m) => ({
                ...m,
                buttons: undefined,
            }))
            if (message instanceof Array) return [...oldMessages, ...message]
            return [...oldMessages, message]
        })
    }

    function removeButtons(id: string) {
        setMessages((messages) => {
            return messages.map((message) => {
                if (message.id === id) return { ...message, buttons: undefined }
                // return { ...message, buttons: undefined }
                return message
            })
        })
    }

    useEffect(() => {
        const scrollPosition = window.pageYOffset
        document.body.style.overflow = 'hidden'
        document.body.style.position = 'fixed'
        document.body.style.top = `-${scrollPosition}px`
        document.body.style.width = '100%'
        document.body.style.background = 'white'
        return () => {
            document.body.style.removeProperty('overflow')
            document.body.style.removeProperty('position')
            document.body.style.removeProperty('top')
            document.body.style.removeProperty('width')
            window.scrollTo(0, scrollPosition)
        }
    }, [])

    async function onCallback(
        callback: string,
        messageId: string,
        text: string
    ) {
        removeButtons(messageId)

        const message = {
            data_type: 'callback',
            id: shortid.generate(),
            author: MessageAuthor.USER,
        }
        onMessage([{ ...message, data: { content: text } } as Message])
        const { messages } = await sendMessage({
            messages: [{ ...message, data: { content: callback } } as any],
            conversation_id: `${conversationId}`,
        })

        onMessage(messages)
    }

    return (
        <ChatLayout>
            <ChatFileMaxSizeModal isOpen={isOpen} onClose={onClose} />
            <ChatHeader />
            <Box className={OrderPageStyles['messages']} ref={messagesRef}>
                {messages.map((message) => (
                    <MessageComponent
                        message={message}
                        onCallback={onCallback}
                        key={message.id}
                    />
                ))}
            </Box>
            {conversationId && (
                <ChatForm
                    conversationId={conversationId}
                    onMessage={onMessage}
                />
            )}
        </ChatLayout>
    )
}

export default OrderPage
