import { Box } from '@chakra-ui/react'
import { GetStaticProps, NextPage } from 'next'
import { getProject, getProjectsList } from '../../api/projects.api'
import Layout from '../../components/Layout/Layout'
import RenderPage from '../../components/RenderPage/RenderPage'
import CasePageStyles from '../../styles/case-page.module.scss'
import PageData from '../../types/PageData'

const CasePage: NextPage<PageData> = (props) => {
    return (
        <Layout
            link="/portfolio"
            pythia={props.pythia_properties}
            container={props.body_properties}
            header={props.header_properties}
        >
            <Box className={CasePageStyles['content']}>
                <Box
                    bgImage={props.rendering_data.cover?.image}
                    className={CasePageStyles['cover']}
                >
                    <div style={{ height: '60vh' }}>
                        <div className={CasePageStyles['cover-content']}>
                            <h1>{props.rendering_data.cover?.title}</h1>
                            <div className={CasePageStyles['cover-tags']}>
                                {props.rendering_data.cover?.tags?.map(
                                    (tag: string) => (
                                        <div key={tag}>#{tag}</div>
                                    )
                                )}
                            </div>
                        </div>
                    </div>
                </Box>
                <RenderPage elements={props.rendering_data.content} />
            </Box>
        </Layout>
    )
}

export async function getStaticPaths() {
    try {
        const list = await getProjectsList()
        const paths = list.map(({ id }) => ({ params: { name: id } }))
        return {
            paths,
            fallback: false,
        }
    } catch (e) {
        return {
            paths: [],
            fallback: false,
        }
    }
}

export const getStaticProps: GetStaticProps = async (
    context
): Promise<{ props: PageData }> => {
    const { name: id } = context.params as { name: string }
    const data = await getProject(id)

    return {
        props: data,
    }
}

export default CasePage
