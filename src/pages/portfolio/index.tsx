import {
    Grid,
    Box,
    GridItem,
    Flex,
    Text,
    Link,
    Spinner,
    Button,
} from '@chakra-ui/react'
import type { NextPage } from 'next'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import Layout from '../../components/Layout/Layout'
import NextLink from 'next/link'
import Select from '../../components/Select/Select'
import { getProjects } from '../../api/projects.api'
import PortfolioStyles from '../../styles/portfolio.module.scss'
import ProjectCard from '../../types/ProjectCard.type'
import formatColor from '../../lib/formatColor'
import InfiniteScroll from 'react-infinite-scroll-component'
import { useInfiniteQuery } from 'react-query'

interface SSRProps {
    data: ProjectCard[]
    nextPage: boolean
}

const Portfolio: NextPage<SSRProps> = (props) => {
    const { t } = useTranslation()
    const [page, setPage] = useState(1)
    const [tech, setTech] = useState('')
    const [sector, setSector] = useState('')

    const { data, fetchNextPage, hasNextPage } = useInfiniteQuery(
        ['projects', tech, sector],
        ({ pageParam = 1 }) => {
            return getProjects({
                page: pageParam,
                tech,
                sector,
            })
        },
        {
            getNextPageParam: (lastPage) => lastPage.nextPage,
            initialData: {
                pageParams: [],
                pages: [props],
            },
        }
    )

    const gridStyles = {
        large: {
            colSpan: 10,
            rowSpan: 1,
        },
        small: {
            rowSpan: 1,
            colSpan: 3,
        },
        middle: {
            colSpan: 5,
            rowSpan: 1,
        },
        'pre-middle': {
            colSpan: 6,
            rowSpan: 1,
        },
        side: {
            colSpan: 4,
            rowSpan: 1,
        },
    }

    function chooseSector({ key }: any) {
        setPage(1)
        setSector(key)
    }

    function chooseTech({ key }: any) {
        setPage(1)
        setTech(key)
    }

    function clearFilters() {
        setPage(1)
        setTech('')
        setSector('')
    }

    return (
        <Layout
            pythia={{
                bgColor: '#CCFF33',
            }}
        >
            <Flex
                flexDirection="column"
                textAlign="center"
                alignItems="center"
                justifyContent="center"
                padding="5% 5% 2.5% 5%"
            >
                <Text className={PortfolioStyles['title']}>
                    {t('portfolio.our_work')}
                </Text>
                <Text className={PortfolioStyles['subtitle']}>
                    {t('portfolio.team')}
                </Text>
                <Flex
                    className={PortfolioStyles['filters']}
                    justifyContent="center"
                >
                    <Select
                        options={[].map(({ name, icon }) => ({
                            key: name,
                            value: name,
                            icon,
                        }))}
                        buttonStyles={{ outline: '0.5px solid silver' }}
                        onChange={chooseTech}
                        value={tech}
                        placeholder={t('portfolio.technologies')}
                    />
                    <Select
                        options={[].map(({ name, icon }) => ({
                            key: name,
                            value: name,
                            icon,
                        }))}
                        buttonStyles={{ outline: '0.5px solid silver' }}
                        onChange={chooseSector}
                        value={sector}
                        placeholder={t('portfolio.industries')}
                    />
                    {(sector || tech) && (
                        <Button
                            onClick={clearFilters}
                            variant="outline"
                            colorScheme="red"
                        >
                            Очистить фильтры
                        </Button>
                    )}
                </Flex>
            </Flex>
            <InfiniteScroll
                next={() => {
                    setPage((page) => page + 1)
                    fetchNextPage({ pageParam: page + 1 })
                }}
                dataLength={(data?.pages.length || 0) * 10}
                hasMore={Boolean(hasNextPage)}
                loader={
                    <p style={{ textAlign: 'center' }}>
                        <Spinner marginY="1%" size="lg" />
                    </p>
                }
            >
                <Grid
                    templateRows="repeat(1, 1fr)"
                    templateColumns="repeat(10, 1fr)"
                    className={PortfolioStyles['content-grid']}
                >
                    {data?.pages.map((page) => {
                        return (page.data || []).map((project) => {
                            const grid = (gridStyles as any)[project.size]

                            const overlay = formatColor({
                                ...project.overlay_color,
                                a: 0.6,
                            })
                            const titleColor = formatColor(
                                project.title_font_color
                            )
                            const tagBg = formatColor(project.tag_bg_color)
                            const tagColor = formatColor(project.tag_font_color)

                            const url = new URL(project.bg_image_url)
                            const isVideo =
                                url.searchParams.get('media_unit') === 'videos'

                            return (
                                <NextLink
                                    href={`/portfolio/${project.case_id}`}
                                    shallow
                                    key={project.id}
                                >
                                    <GridItem
                                        className={PortfolioStyles['card']}
                                        {...grid}
                                    >
                                        {isVideo ? (
                                            <video
                                                muted={true}
                                                playsInline
                                                src={project.bg_image_url}
                                                autoPlay
                                                loop
                                            />
                                        ) : (
                                            <img
                                                src={project.bg_image_url}
                                                alt={project.title}
                                            />
                                        )}
                                        <Flex
                                            alignItems="center"
                                            flexWrap="wrap"
                                            marginTop="10px"
                                            position="absolute"
                                            top={0}
                                            left={2.5}
                                        >
                                            {project.tags.map((tag: any) => (
                                                <Box
                                                    background={tagBg}
                                                    key={tag}
                                                    userSelect="none"
                                                    marginRight="10px"
                                                    borderRadius="5px"
                                                    padding="2.5px 5px"
                                                    color={tagColor}
                                                    fontWeight={700}
                                                    fontFamily="'Mulish', sans-serif"
                                                >
                                                    #{tag}
                                                </Box>
                                            ))}
                                        </Flex>
                                        <Box
                                            w="100%"
                                            background={`linear-gradient(0deg, ${overlay} 10%, ${overlay} 0%, transparent 100%)`}
                                            color="white"
                                            position="absolute"
                                            padding="20px 15px"
                                            bottom="0"
                                            left="0"
                                            fontSize="24px"
                                            borderRadius="0px 0px 7px 7px"
                                        >
                                            <Link
                                                fontFamily="'Mulish', sans-serif"
                                                color={titleColor}
                                            >
                                                {project.title}
                                            </Link>
                                        </Box>
                                    </GridItem>
                                </NextLink>
                            )
                        })
                    })}
                </Grid>
            </InfiniteScroll>
        </Layout>
    )
}

export async function getStaticProps(): Promise<{ props: SSRProps }> {
    const data = await getProjects({ page: 1 })
    return {
        props: data,
    }
}

export default Portfolio
