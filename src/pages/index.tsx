import {
    Avatar,
    AvatarGroup,
    Box,
    Button,
    Flex,
    Image,
    Text,
} from '@chakra-ui/react'
import type { NextPage } from 'next'
import { useEffect } from 'react'
import Layout from '../components/Layout/Layout'
import MainPageStyles from '../styles/main.module.scss'

const MainPage: NextPage = () => {
    // https://codepen.io/mmbotelho/pen/NLRLYN

    function spinnageddonScroll() {
        const compensation = window.innerHeight / 1.5
        const position = document.documentElement.scrollTop

        const spinnageddon = document.querySelector(
            '#spinnageddon'
        ) as HTMLElement

        const spinnageddonIcons = document.querySelector(
            '#spins'
        ) as HTMLElement
        const spinnageddonIconItems: NodeListOf<HTMLImageElement> =
            document.querySelectorAll('#spin') || []

        const spinnageddonText = document.querySelector(
            '#spin-text'
        ) as HTMLElement

        const spinnageddonStart =
            (document.querySelector('#spin-start') as any).offsetTop -
            compensation
        const spinnageddonEnd =
            (document.querySelector('#spin-end') as any).offsetTop -
            compensation

        const isCenter =
            position - compensation >= spinnageddonStart + compensation

        const differenceEnd =
            spinnageddonEnd - compensation - (position - compensation)

        const loading = 'https://remix.run//loading.gif'
        const wave = 'https://remix.run/wave.png'

        if (!spinnageddon) return

        if (position < spinnageddonStart) {
            spinnageddon.style.display = 'none'
        }

        if (position >= spinnageddonStart) {
            spinnageddon.style.display = 'block'
        }

        spinnageddonIconItems.forEach((icon) => {
            const src = icon.src
            if (isCenter && src !== wave) {
                icon.src = wave
            }
            if (isCenter) {
                icon.style.transform = `scale(${differenceEnd / 450})`
                icon.style.opacity = `${differenceEnd / 100}`
            }
            if (!isCenter && src !== loading) {
                icon.src = loading
            }
        })

        if (position > spinnageddonEnd) {
            spinnageddonText.style.position = 'block !important'
            spinnageddonIcons.style.display = 'none'
            spinnageddonText.style.display = 'none'
        } else {
            spinnageddonText.style.display = spinnageddonIcons.style.display =
                'block'
            spinnageddonText.style.position = 'fixed'
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', spinnageddonScroll)
        return () => window.removeEventListener('scroll', spinnageddonScroll)
    }, [])

    return (
        <Layout
            container={{ bg: '#121212', fontColor: 'white' }}
            header={{
                bgColor: '#121212',
                fontColor: 'white',
                activeFontColor: 'white',
            }}
        >
            <Flex
                minH="90vh"
                alignItems="center"
                className={MainPageStyles['container']}
            >
                <Box w="100%">
                    <Text w="80%" className={MainPageStyles['title']}>
                        Focused on web{' '}
                        <span className="cyan">fundamentals</span> and{' '}
                        <span className="teal">modern</span> UX, youre simply
                        going to{' '}
                        <span className="yellow">build better websites</span>
                    </Text>
                    <Text className={MainPageStyles['subtitle']}>
                        Remix is a full stack web framework that lets you focus
                        on the user interface and work back through web
                        fundamentals to deliver a fast, slick, and resilient
                        user experience. People are gonna love using your stuff.
                    </Text>
                    <Flex className={MainPageStyles['button-group']}>
                        <Button
                            borderWidth="2px"
                            variant="outline"
                            _hover={{ borderColor: '#3992ff' }}
                            _active={{ bg: 'transparent' }}
                        >
                            Read the docs
                        </Button>
                        <Button variant="filled" bg="#3992ff">
                            Get Started
                        </Button>
                    </Flex>
                </Box>
                <Image borderRadius="7px" src="/assets/code.png" />
            </Flex>

            <Flex
                flexDirection="column"
                marginY="5%"
                alignItems="center"
                className={MainPageStyles['testimonial']}
            >
                <Flex alignItems="center">
                    <AvatarGroup size="md">
                        <Avatar
                            border="none"
                            name="Jenna Smith"
                            src="https://remix.run/img/jenna.jpg"
                            alt="jenna"
                        />
                        <Avatar
                            border="none"
                            name="Twitter"
                            src="https://cdn-icons-png.flaticon.com/512/124/124021.png"
                            alt="twitter"
                        />
                    </AvatarGroup>
                    <Box marginLeft="30px">
                        <Text fontSize="30px" fontWeight="800">
                            Jenna Smith
                        </Text>
                        <Text>RADIX UI</Text>
                    </Box>
                </Flex>
                <h3
                    style={{
                        textAlign: 'center',
                        width: '100%',
                        marginTop: '10px',
                    }}
                >
                    Ive been waiting for something to encourage progressive
                    enhancement in the React space *forever* and Remix truly is
                    so much more. Proving we dont need to sacrifice React or
                    choose SSG for a lightning fast, accessible UI, and the DX
                    makes it all too easy 🤤
                </h3>
            </Flex>

            <Flex className={MainPageStyles['carousel']}>
                <Box>
                    <Flex alignItems="center" justifyContent="space-between">
                        <Flex alignItems="center">
                            <Avatar
                                src="https://remix.run//img/jkup.jpg"
                                alt="jkup"
                                size="md"
                            />
                            <Box marginLeft="15px">
                                <Text fontWeight="800">@jkup</Text>
                                <Text>Cloudflare</Text>
                            </Box>
                        </Flex>
                        <Avatar
                            src="https://cdn-icons-png.flaticon.com/512/124/124021.png"
                            alt="124021.png"
                            size="sm"
                        />
                    </Flex>
                    <Text
                        color="rgb(229 231 235)"
                        marginTop="20px"
                        fontWeight="400"
                    >
                        My mind is still blown away with Remix! So easy and
                        elegant 😩. I love how it also focuses on Accessibility
                        (Progressive Enhancement...) 🤯, A few days ago I was
                        like wazzup with remix, we got Next.js and Svelte 😴 ...
                        and now Im fired up like crazy 😂. This is so good 🤤
                    </Text>
                </Box>
                <Box
                    minW="25%"
                    marginRight="20px"
                    bg="#1e1e1e"
                    h="100%"
                    borderRadius="7px"
                    py="20px"
                    px="40px"
                >
                    <Flex alignItems="center" justifyContent="space-between">
                        <Flex alignItems="center">
                            <Avatar
                                src="https://remix.run//img/jkup.jpg"
                                alt="jkup"
                                size="md"
                            />
                            <Box marginLeft="15px">
                                <Text fontWeight="800">@jkup</Text>
                                <Text>Cloudflare</Text>
                            </Box>
                        </Flex>
                        <Avatar
                            src="https://cdn-icons-png.flaticon.com/512/124/124021.png"
                            size="sm"
                            alt="124021.png"
                        />
                    </Flex>
                    <Text
                        color="rgb(229 231 235)"
                        marginTop="20px"
                        fontWeight="400"
                    >
                        My mind is still blown away with Remix! So easy and
                        elegant 😩. I love how it also focuses on Accessibility
                        (Progressive Enhancement...) 🤯, A few days ago I was
                        like wazzup with remix, we got Next.js and Svelte 😴 ...
                        and now Im fired up like crazy 😂. This is so good 🤤
                    </Text>
                </Box>
                <Box
                    minW="25%"
                    marginRight="20px"
                    bg="#1e1e1e"
                    h="100%"
                    borderRadius="7px"
                    py="20px"
                    px="40px"
                >
                    <Flex alignItems="center" justifyContent="space-between">
                        <Flex alignItems="center">
                            <Avatar
                                src="https://remix.run//img/jkup.jpg"
                                alt="jkup"
                                size="md"
                            />
                            <Box marginLeft="15px">
                                <Text fontWeight="800">@jkup</Text>
                                <Text>Cloudflare</Text>
                            </Box>
                        </Flex>
                        <Avatar
                            src="https://cdn-icons-png.flaticon.com/512/124/124021.png"
                            size="sm"
                            alt="124021.png"
                        />
                    </Flex>
                    <Text
                        color="rgb(229 231 235)"
                        marginTop="20px"
                        fontWeight="400"
                    >
                        My mind is still blown away with Remix! So easy and
                        elegant 😩. I love how it also focuses on Accessibility
                        (Progressive Enhancement...) 🤯, A few days ago I was
                        like wazzup with remix, we got Next.js and Svelte 😴 ...
                        and now Im fired up like crazy 😂. This is so good 🤤
                    </Text>
                </Box>
                <Box
                    minW="25%"
                    marginRight="20px"
                    bg="#1e1e1e"
                    h="100%"
                    borderRadius="7px"
                    py="20px"
                    px="40px"
                >
                    <Flex alignItems="center" justifyContent="space-between">
                        <Flex alignItems="center">
                            <Avatar
                                src="https://remix.run//img/jkup.jpg"
                                size="md"
                                alt="jkup"
                            />
                            <Box marginLeft="15px">
                                <Text fontWeight="800">@jkup</Text>
                                <Text>Cloudflare</Text>
                            </Box>
                        </Flex>
                        <Avatar
                            src="https://cdn-icons-png.flaticon.com/512/124/124021.png"
                            size="sm"
                            alt="124021.png"
                        />
                    </Flex>
                    <Text
                        color="rgb(229 231 235)"
                        marginTop="20px"
                        fontWeight="400"
                    >
                        My mind is still blown away with Remix! So easy and
                        elegant 😩. I love how it also focuses on Accessibility
                        (Progressive Enhancement...) 🤯, A few days ago I was
                        like wazzup with remix, we got Next.js and Svelte 😴 ...
                        and now Im fired up like crazy 😂. This is so good 🤤
                    </Text>
                </Box>
            </Flex>
            <Text className={MainPageStyles['heading']}>
                While you were <span className="red">waiting</span> for your
                static site to build,{' '}
                <span className="blue">distributed web</span> infrastructure got
                really good.{' '}
                <span className="pink">Break through the static.</span>
            </Text>
            <Text className={MainPageStyles['text']}>
                Remix is a seamless server and browser runtime that provides
                snappy page loads and instant transitions by leveraging
                distributed systems and native browser features instead of
                clunky static builds. Built on the Web Fetch API (instead of
                Node) <span className="white">it can run anywhere</span>. It
                already runs natively on Cloudflare Workers, and of course
                supports serverless and traditional Node.js environments, so you
                can come as you are.
            </Text>
            <Text className={MainPageStyles['text']}>
                Page speed is only one aspect of our true goal though. Were
                after <span className="white">better user experiences</span>. As
                youve pushed the boundaries of the web, your tools havent caught
                up to your appetite.{' '}
                <span className="white">Remix is ready </span> to serve you from
                the initial request to the fanciest UX your designers can think
                up. Check it out 👀
            </Text>
            <Box className={MainPageStyles['compare-container']}>
                <Flex
                    justifyContent="center"
                    className={MainPageStyles['compare']}
                >
                    <Box>
                        <Text>Without Remix</Text>
                        <Image src="/assets/sticky-image.png" />
                    </Box>
                    <Box>
                        <Text>With Remix</Text>
                        <Image src="/assets/sticky-image.png" />
                    </Box>
                </Flex>
            </Box>

            <Box id="spin-start" />
            <Box h="200vh">
                <Box
                    className={MainPageStyles['spinnageddon']}
                    id="spinnageddon"
                >
                    <Box
                        className={MainPageStyles['spinnageddon-icons']}
                        id="spins"
                    >
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="75px"
                            right="40px"
                            top="100px"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="120px"
                            right="10%"
                            bottom="30%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="150px"
                            left="5%"
                            bottom="10%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="200px"
                            left="30%"
                            top="10%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="75px"
                            left="50%"
                            top="40%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="75px"
                            left="5%"
                            top="5%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="100px"
                            left="12.5%"
                            top="25%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="100px"
                            right="22.5%"
                            bottom="10%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="100px"
                            right="12.5%"
                            top="25%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="100px"
                            left="40%"
                            bottom="27%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="140px"
                            right="35%"
                            top="15%"
                        />
                        <Image
                            src="https://remix.run/wave.png"
                            id="spin"
                            width="140px"
                            left="30%"
                            bottom="5%"
                        />
                    </Box>
                    <Text
                        className={MainPageStyles['spinnageddon-text']}
                        id="spin-text"
                    >
                        Say goodbye to Spinnageddon
                    </Text>
                </Box>
            </Box>
            <Box id="spin-end" />

            <Text className={MainPageStyles['h1']}>
                Remix has a cheat code:{' '}
                <span className="yellow">Nested Routes.</span>{' '}
                <span className="gray">↑↑↓↓←→←→BA</span>
            </Text>
            <Box w="60%" marginY="2.5%" className={MainPageStyles['container']}>
                <Box id="nestedRoutesBreakpoints" w="80%">
                    <h2>
                        Websites usually have levels of navigation that control
                        child views.
                    </h2>
                    <h2>
                        Not only are these components pretty much always coupled
                        to URL segments...
                    </h2>
                    <h2>
                        ...theyre also the semantic boundary of data loading and
                        code splitting.
                    </h2>
                    <h2>
                        Hover or tap the buttons to see how theyre all related
                    </h2>
                </Box>
                <Box
                    position="sticky"
                    bottom="-5vh"
                    className={MainPageStyles['sticky']}
                >
                    <Image
                        id="nestedRoutesImage"
                        src="/assets/sticky-image.png"
                        display="block"
                    />
                    <Image
                        id="nestedRoutesImage"
                        src="/assets/code.png"
                        display="none"
                    />
                    <Image
                        id="nestedRoutesImage"
                        src="/assets/sticky-image.png"
                        position="sticky"
                        bottom="0"
                        display="none"
                    />
                    <Image
                        id="nestedRoutesImage"
                        src="/assets/sticky-image.png"
                        position="sticky"
                        bottom="0"
                        display="none"
                    />
                    <Image
                        id="nestedRoutesImage"
                        src="/assets/sticky-image.png"
                        position="sticky"
                        bottom="0"
                        display="none"
                    />
                </Box>
            </Box>
            <Text className={MainPageStyles['h1']}>
                Through nested routes, Remix can eliminate nearly{' '}
                <span className="teal">every loading state.</span>
            </Text>
            <Text className={MainPageStyles['heading']}>
                Most web apps fetch inside of components, creating{' '}
                <span className="cyan">request waterfalls</span>, slower loads,
                and <span className="red">jank</span>.
            </Text>
            <Text className={MainPageStyles['heading']}>
                Remix loads data in parallel on the server and sends a fully
                formed HTML document.{' '}
                <span className="pink">Way faster, jank free.</span>
            </Text>
            <Text className={MainPageStyles['h1']}>
                Nested routes allow Remix to make your app{' '}
                <span className="red">as fast as instant</span>
            </Text>

            <Box w="60%" marginY="2.5%" className={MainPageStyles['container']}>
                <Box w="80%">
                    <h2>
                        Remix can prefetch everything in parallel before the
                        user clicks a link.
                    </h2>
                    <h2>Public Data. User Data. Modules. Heck, even CSS.</h2>
                    <h2>
                        ...theyre also the semantic boundary of data loading and
                        code splitting.
                    </h2>
                    <h2>Zero loading states. Zero skeleton UI. Zero jank.</h2>
                    <h2 className="gray">
                        Alright, you caught us, theyre just prefetch link tags,
                        #useThePlatform
                    </h2>
                </Box>
                <Image
                    src="/assets/sticky-image.png"
                    className={MainPageStyles['sticky-image']}
                />
            </Box>
            <Text className={MainPageStyles['heading']}>
                Data loading ... 🥱 <br />
                You ever notice most of the code in your app is for{' '}
                <span className="yellow">changing data?</span>
            </Text>
            <Text className={MainPageStyles['text']}>
                Imagine if React only had props and no way to set state. Whats
                the point? If a web framework helps you load data but doesnt
                help you update it, whats the point? Remix doesnt drop you off
                at the {'<form onSubmit>'} cliff. (What the heck does
                event.preventDefault do anyway?)
            </Text>

            <Text className={MainPageStyles['h1']}>
                Resilient, progressively enhanced{' '}
                <span className="blue">data updates</span> are built in.
            </Text>
            <Flex p="10% 0" margin="auto" className={MainPageStyles['flex']}>
                <Box w="60%">
                    <Text w="60%" className={MainPageStyles['heading']}>
                        Its so simple its kind of silly. Just make a form...
                    </Text>
                    <Text w="60%" className={MainPageStyles['heading']}>
                        ...and an action on the server. The whole thing works
                        with no JavaScript!
                    </Text>
                    <Text w="60%" className={MainPageStyles['heading']}>
                        Remix runs the action server side, revalidates data
                        client side, and even handles race conditions from
                        resubmissions.
                    </Text>
                    <Text w="60%" className={MainPageStyles['heading']}>
                        Get fancy with transition hooks and make some pending
                        UI. Remix handles all the state, you simply ask for it.
                    </Text>
                    <Text w="60%" className={MainPageStyles['heading']}>
                        Or get jiggy with some optimistic UI. Remix provides the
                        data being sent to the server so you can skip the busy
                        spinners for mutations, too.
                    </Text>
                    <Text w="60%" className={MainPageStyles['heading']}>
                        HTML Forms. Who knew?
                    </Text>
                </Box>
                <Flex
                    h="100vh"
                    position="sticky"
                    width="50%"
                    top="5vh"
                    bg="#1e1e1e"
                    alignItems="center"
                    justifyContent="center"
                    className={MainPageStyles['flex-sticky']}
                >
                    <Image src="/assets/code-example.png" h="50%" />
                </Flex>
            </Flex>

            <Box h="200vh">
                <Image
                    id="windows-error"
                    src="http://remix.run/busted.jpg"
                    w="100%"
                    h="100vh"
                    position="sticky"
                    top="0"
                />
            </Box>

            <Box bg="#3992ff" h="200vh">
                <Flex
                    className={MainPageStyles['windows']}
                    h="100vh"
                    padding="0 5%"
                    justifyContent="center"
                    position="sticky"
                    top="10vh"
                    flexDirection="column"
                >
                    <Text fontWeight="400" fontSize="100px">
                        :)
                    </Text>
                    <Text fontWeight="100">
                        Your websites run into problems, but with Remix they
                        don’t need to be refreshed. Error handling is hard to
                        remember and hard to do. That’s why it’s built in.
                    </Text>
                    <Box margin=".5% 0" />
                    <Text fontWeight="400">
                        Remix handles errors while Server Rendering. Errors
                        while Client Rendering. Even errors in your server side
                        data handling.
                    </Text>
                </Flex>
            </Box>

            <Text className={MainPageStyles['h1']}>
                Route Error Boundaries{' '}
                <span className="yellow">keep the happy path happy.</span>
            </Text>
            <Box>
                <Box className={MainPageStyles['container']} w="60%">
                    <h2>
                        Each route module can export an error boundary next to
                        the default route component.
                    </h2>
                    <h2>
                        If an error is thrown, client or server side, users see
                        the boundary instead of the default component.
                    </h2>
                    <h2>
                        Routes w/o troule render normally, so users have more
                        options than slamming refresh.
                    </h2>
                    <h2>
                        If a route has no boundary, errors bubble up. Just put
                        one at the top and chill out about errors in code
                        review, yeah?
                    </h2>
                </Box>
                <Image
                    src="/assets/code-example1.png"
                    position="sticky"
                    bottom="0"
                />
            </Box>
            <Text className={MainPageStyles['h1']}>
                Thats probably enough for now. What are you waiting for?
            </Text>
            <Box className={MainPageStyles['container']} w="65%">
                <Button
                    borderWidth="2px"
                    borderColor="#3992ff"
                    variant="filled"
                    bg="#3992ff"
                    marginTop="15px"
                >
                    Go Play
                </Button>
            </Box>
        </Layout>
    )
}

export default MainPage
