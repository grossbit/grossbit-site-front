import {
    Box,
    Button,
    Flex,
    Grid,
    GridItem,
    Image,
    Input,
    Text,
    Textarea,
} from '@chakra-ui/react'
import {
    Facebook,
    Instagram,
    Linkedin,
    Mail,
    MapPin,
    Phone,
    Twitter,
} from 'lucide-react'
import type { NextPage } from 'next'
import Layout from '../components/Layout/Layout'
import AboutPageStyles from '../styles/about-page.module.scss'
import GoogleMap from 'google-map-react'

const data = [
    {
        title: 'Кто мы',
        text: 'Мы - студия разработки, которая сделает ваш продукт не только успешным, а еще легкоподдерживаемым!',
        image: '',
        background: '#e7e2d6',
        color: 'black',
        size: 'small',
    },
    {
        title: 'Задачи которые мы выполняем',
        text: `За время существования студии, нашей командой было создано большое количество проектов и решенено не мало проблем. 
Мы умеем: создавать ваш проект с нуля, настраивать CI для автоматизации тестирования продукта, серверную и клиентскую часть и так далее...`,
        image: '',
        background: '#f3dbc5',
        color: 'black',
        size: 'small',
    },
    {
        image: 'https://bissoft.org/images/company.png',
        background: '#e7e5ff',
        color: 'black',
        size: 'large',
        title: 'Команда',
        text: 'В нашу команду входят добрые, отзывчивые, а самое главное ответственные разработчики, которые в любое для Вас время свяжуться и обсудять планы',
    },
]
const About: NextPage = () => {
    return (
        <Layout title="О нас">
            <Box className={AboutPageStyles['container']}>
                <Text className={AboutPageStyles['title']}>О нас</Text>
                <Grid
                    templateRows="repeat(3, 1fr)"
                    templateColumns="repeat(2, 1fr)"
                    gap={4}
                    className={AboutPageStyles['cards']}
                >
                    {data.map((info) => (
                        <GridItem
                            className={AboutPageStyles['card']}
                            key={info.title}
                            bg={info.background}
                            colSpan={info.size === 'small' ? 1 : 2}
                            rowSpan={3}
                        >
                            <Box margin="20px">
                                <Text fontSize={24} color={info.color}>
                                    {info.title}
                                </Text>
                                <Text
                                    marginTop="5px"
                                    whiteSpace="pre-wrap"
                                    color={info.color}
                                >
                                    {info.text}
                                </Text>
                            </Box>
                            {info.image ? (
                                <Image alt={info.image} src={info.image} />
                            ) : null}
                        </GridItem>
                    ))}
                </Grid>
                <Box className={AboutPageStyles['map']}>
                    <GoogleMap
                        bootstrapURLKeys={{
                            key: 'AIzaSyDy9wgc6n6mHSkNnzpfCBjNsI02rR0CHLg',
                        }}
                        defaultCenter={{ lat: 59.95, lng: 30.33 }}
                        zoom={11}
                    />
                </Box>
                <Flex className={AboutPageStyles['form']}>
                    <Box className={AboutPageStyles['form-info']}>
                        <Box>
                            <Text
                                className={AboutPageStyles['form-info-title']}
                            >
                                Contact form
                            </Text>
                            <Text
                                className={
                                    AboutPageStyles['form-info-subtitle']
                                }
                            >
                                Fill up the form and our Team will get back to
                                you
                            </Text>
                        </Box>
                        <Box className={AboutPageStyles['form-info-contacts']}>
                            <Flex alignItems="center">
                                <Phone fill="#fa949d" color="#3e1f92" />
                                <Text>+700000000</Text>
                            </Flex>
                            <Flex alignItems="center">
                                <Mail fill="#fa949d" color="#3e1f92" />
                                <Text>example@gmail.com</Text>
                            </Flex>
                            <Flex alignItems="center">
                                <MapPin fill="#fa949d" color="#3e1f92" />
                                <Text>Dnepr, Ukraine</Text>
                            </Flex>
                        </Box>
                        <Flex className={AboutPageStyles['form-social']}>
                            <a href="">
                                <Facebook strokeWidth="1.25px" />
                            </a>
                            <a href="">
                                <Twitter strokeWidth="1.25px" />
                            </a>
                            <a href="">
                                <Instagram strokeWidth="1.25px" />
                            </a>
                            <a href="">
                                <Linkedin strokeWidth="1.25px" />
                            </a>
                        </Flex>
                    </Box>
                    <Box className={AboutPageStyles['form-inputs']}>
                        <Text mb="8px">Your name</Text>
                        <Input
                            variant="flushed"
                            focusBorderColor="#3e1f92"
                            placeholder="Your name"
                        />
                        <Text mb="8px">Email address</Text>
                        <Input
                            variant="flushed"
                            focusBorderColor="#3e1f92"
                            placeholder="Email address"
                        />
                        <Text mb="8px">Message</Text>
                        <Textarea
                            focusBorderColor="#3e1f92"
                            placeholder="Message"
                        />
                        <Button
                            float="right"
                            width="30%"
                            background="#3e1f92"
                            color="white"
                            _hover={{ background: '#3e1f92' }}
                            _active={{ background: '#3e1f92' }}
                        >
                            Send
                        </Button>
                    </Box>
                </Flex>
            </Box>
        </Layout>
    )
}

export default About
