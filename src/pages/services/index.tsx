import React from 'react'
import NextLink from 'next/link'
import { Text } from '@chakra-ui/react'
import Layout from '../../components/Layout/Layout'
import ServicesPageStyles from '../../styles/services-page.module.scss'
import servicesData from '../../../__mocks__/services.data'

const ServicesPage = () => {
    return (
        <Layout
            container={{
                bg: '#000',
            }}
            header={{
                bgColor: 'dark.800',
                fontColor: 'silver',
                activeFontColor: 'white',
            }}
            pythia={{
                bgColor: '#99FF33',
            }}
        >
            <div className={ServicesPageStyles['container']}>
                <div className={ServicesPageStyles['parallax']}>
                    <Text className={ServicesPageStyles['title']}>
                        Услуги и компетенции
                    </Text>
                    <Text className={ServicesPageStyles['subtitle']}>
                        Ваша задача - наше решение
                    </Text>
                </div>
                <div className={ServicesPageStyles['content']}>
                    {servicesData.map((data) => (
                        <div
                            key={data.name}
                            className={ServicesPageStyles['item']}
                        >
                            <img src={data.icon} />
                            <Text className={ServicesPageStyles['item-title']}>
                                {data.name}
                            </Text>
                            <Text
                                className={ServicesPageStyles['item-subtitle']}
                                textAlign="center"
                                fontSize="md"
                                fontWeight={400}
                            >
                                {data.description}
                            </Text>
                            <NextLink href={data.link} shallow>
                                Узнать больше
                            </NextLink>
                        </div>
                    ))}
                </div>
            </div>
        </Layout>
    )
}

export default ServicesPage
