import Layout from '../../components/Layout/Layout'
import React from 'react'
import ServicesPageStyles from '../../styles/services-page.module.scss'
import {
    Accordion,
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Box,
    Grid,
    GridItem,
    Text,
} from '@chakra-ui/react'
import Testimonial from '../../components/Testimonial/Testimonial'
import LogoClouds from '../../components/LogoClouds/LogoClouds'
import Stepper from '../../components/Stepper/Stepper'

const Product: React.FC = () => {
    return (
        <Layout link="/services">
            <div
                className={ServicesPageStyles['parallax']}
                style={{
                    backgroundImage: `url("https://wallpaperbat.com/img/162989-norway-wallpaper-top-free-norway-background.jpg")`,
                }}
            >
                <Text className={ServicesPageStyles['title']}>
                    IT-продукт под ключ
                </Text>
            </div>
            <div className={ServicesPageStyles['article']}>
                <p className={ServicesPageStyles['text']}>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Eos, rem ex, alias iure maxime impedit vel laboriosam
                    recusandae expedita necessitatibus, qui harum. Corporis
                    laboriosam non itaque sapiente vel quibusdam quod! Deleniti
                    dolorum, accusantium, quo non minus unde esse saepe
                    doloremque molestiae sequi, ratione distinctio. Molestiae
                    obcaecati eius, architecto quam maxime delectus voluptatibus
                    atque corporis aliquam laboriosam veniam, at qui voluptates.
                    Distinctio, incidunt? Itaque quasi maiores esse debitis
                    vitae quod, quas reprehenderit provident, ullam, voluptatum
                    architecto velit quaerat nisi iste eos porro voluptas
                    praesentium assumenda odit minus eveniet nostrum suscipit
                    cumque. Eligendi rem nostrum ea alias suscipit officiis
                    consequatur quidem aperiam aut repellat! Nisi ullam iure
                    placeat recusandae itaque modi debitis consectetur
                    distinctio consequatur nemo. Facilis, amet in. Repellendus,
                    commodi ut. Ex ipsa, totam qui officiis deleniti explicabo
                    nam! Dolorem consequuntur iste accusamus incidunt modi
                    perferendis eaque, nemo architecto totam quae placeat eum,
                    non tempora pariatur error. Expedita sapiente unde
                    architecto. Molestiae perferendis, excepturi ullam
                    exercitationem minus maxime sequi delectus, est ducimus
                    cupiditate incidunt nihil reprehenderit dolorem accusantium
                    possimus, doloremque commodi cumque nemo omnis? Distinctio
                    velit quibusdam animi, laudantium autem nemo. Maxime
                    excepturi, assumenda corrupti voluptas nemo, blanditiis
                    labore velit hic eligendi quam ratione repellat dicta
                    dolores, qui vitae. Officiis blanditiis cumque dolore vero
                    aut voluptate? Vel illum sint mollitia recusandae!
                    Voluptatem, at blanditiis accusantium similique a odit omnis
                    officiis earum porro, amet nam vitae impedit facilis
                    eligendi aliquam quasi cupiditate ullam vel quis error eum
                    enim! Ex repellendus quas atque. Quia tempore delectus
                    nihil, voluptas laboriosam quidem doloribus commodi suscipit
                    vitae, veniam quas atque consectetur voluptatem ex ullam
                    enim officiis voluptate, dolorem recusandae. Quasi sequi
                    itaque, veritatis ullam voluptatum laborum. Repellendus,
                    eum? Voluptatem aperiam dolore dolores tenetur animi atque
                    laudantium ipsum aliquid architecto magnam perferendis
                    labore fugit autem quo qui iure obcaecati nihil dolorem,
                    libero at ducimus accusamus quibusdam sunt.
                </p>
                <div className={ServicesPageStyles['stats']}>
                    <p className={ServicesPageStyles['stats-heading']}>
                        Коротко о нас
                    </p>
                    <p className={ServicesPageStyles['stats-sub']}>
                        Вы можете положиться на нас
                    </p>
                    <Grid
                        templateColumns="repeat(3, 1fr)"
                        className={ServicesPageStyles['stats-data']}
                    >
                        <GridItem colSpan={1}>
                            <h1>99</h1>
                            <h3>Запущенных проектов</h3>
                        </GridItem>
                        <GridItem colSpan={1}>
                            <h1>24/7</h1>
                            <h3>В онлайн режиме</h3>
                        </GridItem>
                        <GridItem colSpan={1}>
                            <h1>140</h1>
                            <h3>Сотрудников, готовых помочь вам</h3>
                        </GridItem>
                    </Grid>
                </div>
                <div className={ServicesPageStyles['heading']}>
                    Часто задаваемые вопросы
                </div>
                <Accordion allowMultiple defaultIndex={[0]} allowToggle>
                    <AccordionItem background="white">
                        <AccordionButton _hover={{ background: 'white' }}>
                            <Box flex="1" textAlign="left" fontSize={26}>
                                Почему стоит обратиться к нам?
                            </Box>
                            <AccordionIcon />
                        </AccordionButton>
                        <AccordionPanel
                            pb={4}
                            className={ServicesPageStyles['text']}
                        >
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat.
                        </AccordionPanel>
                    </AccordionItem>

                    <AccordionItem background="white">
                        <AccordionButton _hover={{ background: 'white' }}>
                            <Box flex="1" fontSize={26} textAlign="left">
                                Что мы используем?
                            </Box>
                            <AccordionIcon />
                        </AccordionButton>
                        <AccordionPanel
                            pb={4}
                            className={ServicesPageStyles['text']}
                        >
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat.
                        </AccordionPanel>
                    </AccordionItem>
                </Accordion>
                <Testimonial />
                <LogoClouds />
                <Stepper />
            </div>
        </Layout>
    )
}

export default Product
