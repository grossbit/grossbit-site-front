import Layout from '../../components/Layout/Layout'
import React from 'react'

const Outsource: React.FC = () => {
    return <Layout link="/services"></Layout>
}

export default Outsource
