import React from 'react'
import { Box, Flex, Grid, GridItem, Text } from '@chakra-ui/react'
import Link from 'next/link'
import Layout from '../../components/Layout/Layout'
import CareersStyles from '../../styles/careers.module.scss'
import { getCareers } from '../../api/careers.api'
import CareerCard from '../../types/CareerCard.type'
interface Props {
    careers: Array<[string, CareerCard[]]>
}

const Careers: React.FC<Props> = ({ careers }) => {
    return (
        <Layout
            container={{
                bg: "url('/assets/vacancy-bg.jpg')",
            }}
            header={{
                bgColor: 'transparent',
                activeFontColor: 'white',
                scrollBgColor: 'white',
                scrollFontColor: 'silver',
                scrollActiveFontColor: 'black',
            }}
            pythia={{
                bgColor: '#CC99FF',
            }}
        >
            <Box>
                <Box w="100%" textAlign="center" paddingTop="3%">
                    <Text className={CareersStyles['title']}>Вакансии</Text>
                    <Text className={CareersStyles['subtitle']}>
                        Ознакомьтесь кто мы, что мы делаем и почему мы делаем.
                    </Text>
                </Box>
                <Box className={CareersStyles['cards']}>
                    {careers.map(([title, careers]) => (
                        <>
                            <Text className={CareersStyles['heading']}>
                                {title}
                            </Text>
                            <Grid templateColumns="repeat(3, 1fr)">
                                {careers.map((career) => (
                                    <Link
                                        passHref
                                        key={career.id}
                                        href={`/careers/${career.vacancy_id}`}
                                        shallow
                                    >
                                        <GridItem
                                            colSpan={1}
                                            className={CareersStyles['card']}
                                        >
                                            <Flex
                                                h="100%"
                                                flexDirection="column"
                                                justifyContent="space-between"
                                            >
                                                <Text
                                                    className={
                                                        CareersStyles[
                                                            'card-title'
                                                        ]
                                                    }
                                                >
                                                    {career.title}
                                                </Text>
                                                <Flex justifyContent="space-between">
                                                    <Text
                                                        className={
                                                            CareersStyles[
                                                                'card-additional'
                                                            ]
                                                        }
                                                    >
                                                        {career.geo_place}
                                                    </Text>
                                                    <svg
                                                        width="40"
                                                        height="40"
                                                        viewBox="0 0 80 80"
                                                    >
                                                        <path
                                                            fill="currentColor"
                                                            fillRule="evenodd"
                                                            clipRule="evenodd"
                                                            d="M53.3337 20H70.0003C71.8437 20 73.3337 21.4933 73.3337 23.3333V40C73.3337 41.84 71.8437 43.3333 70.0003 43.3333H46.667V40H33.3337V43.3333H10.0003C8.16033 43.3333 6.66699 41.84 6.66699 40V23.3333C6.66699 21.4933 8.16033 20 10.0003 20H26.667V9.99999C26.667 8.15999 28.1603 6.66666 30.0003 6.66666H50.0003C51.8437 6.66666 53.3337 8.15999 53.3337 9.99999V20ZM33.3337 20H46.667V13.3333H33.3337V20ZM33.3333 53.3333H46.6667V46.6667H70V66.6667C70 70.3433 67.0133 73.3333 63.3333 73.3333H16.6667C12.9933 73.3333 10 70.3433 10 66.6667V46.6667H33.3333V53.3333Z"
                                                        />
                                                    </svg>
                                                </Flex>
                                            </Flex>
                                        </GridItem>
                                    </Link>
                                ))}
                            </Grid>
                        </>
                    ))}
                </Box>
            </Box>
        </Layout>
    )
}

export async function getStaticProps(): Promise<{ props: Props }> {
    const careers = await getCareers()
    return {
        props: {
            careers,
        },
    }
}

export default Careers
