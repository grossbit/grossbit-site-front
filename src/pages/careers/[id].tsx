import { GetStaticProps } from 'next'
import { useRouter } from 'next/router'
import React, { useRef, useState } from 'react'
import { Box, Button, Input, Text, Textarea, useToast } from '@chakra-ui/react'
import Layout from '../../components/Layout/Layout'
import RenderPage from '../../components/RenderPage/RenderPage'
import CareerStyles from '../../styles/career.module.scss'
import PageData from '../../types/PageData'
import { createOffer, getCareer, getCareersList } from '../../api/careers.api'
import { isEmail } from '../../lib/validation'
import { uploadFile } from '../../api/fs.api'

interface FormState {
    firstname: string
    lastname: string
    email: string
    phone: string
    message: string
    file: File | null
}

type FormError = { [key in keyof Omit<FormState, 'file'>]: string | null }

const Career: React.FC<PageData> = (props) => {
    const { query } = useRouter()
    const toast = useToast()
    const [form, setForm] = useState<FormState>({
        firstname: '',
        lastname: '',
        email: '',
        phone: '',
        message: '',
        file: null,
    })

    const [errors, setErrors] = useState<FormError>({
        firstname: '',
        lastname: '',
        email: '',
        phone: '',
        message: '',
    })

    const fileInputRef = useRef<HTMLInputElement | null>(null)

    const handleChange: React.ChangeEventHandler<
        HTMLInputElement | HTMLTextAreaElement
    > = (e) => {
        const { name, value } = e.target
        setForm({
            ...form,
            [name]: value,
        })
    }

    const submit: React.FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault()
        const validateErrors: FormError = {
            email: '',
            firstname: '',
            lastname: '',
            phone: '',
            message: '',
        }
        if (!isEmail(form.email) || form.email.length === 0) {
            validateErrors['email'] = 'Wrong email address'
        }
        if (form.firstname.length === 0) {
            validateErrors['firstname'] = "First name doesn't exists"
        }
        if (form.lastname.length === 0) {
            validateErrors['lastname'] = "Last name doesn't exists"
        }
        if (form.phone.length < 5) {
            validateErrors['phone'] = "Phone number doesn't exists"
        }

        const errors = Object.values(validateErrors).filter(Boolean)

        if (errors.length) {
            return setErrors(validateErrors)
        }

        const vacancy_id = query.slug as string

        const { file_url } = form.file
            ? await uploadFile(form.file as File)
            : { file_url: null }

        const data = await createOffer({
            vacancy_id,
            resume_url: file_url || '',
            message: form.message,
            email: form.email,
            lastname: form.lastname,
            firstname: form.firstname,
            phone: form.phone,
        })
        toast({
            title: data.id
                ? 'Resume was sent successfully'
                : 'Error with uploading CV',
            status: data.id ? 'success' : 'error',
            position: 'top',
        })
        if (data.id) {
            const empty = {
                email: '',
                file: null,
                firstname: '',
                lastname: '',
                phone: '',
                message: '',
            }
            console.log(empty)
            setForm(empty)
            setErrors(empty)
        }
    }

    const handlePhoneInput: React.KeyboardEventHandler<HTMLInputElement> = (
        e
    ) => {
        const { key } = e
        const isBackspace = key.toLowerCase() === 'backspace'
        const isDigit = Number.isInteger(+e.key)
        const isPlus = key === '+' && !e.currentTarget.value.includes('+')

        if (!isDigit && !isBackspace && !isPlus) e.preventDefault()
    }

    const handleTextInput: React.KeyboardEventHandler<HTMLInputElement> = (
        e
    ) => {
        const key = e.charCode
            ? e.charCode
            : e.keyCode
            ? e.keyCode
            : e.which
            ? e.which
            : e.code?.charCodeAt(0) || 0
        const isAllowedCharacter =
            (key >= 65 && key <= 90) || key === 8 || key === 32 || key === 173
        if (!isAllowedCharacter) e.preventDefault()
    }

    function handleDragOver(evt: any) {
        evt.stopPropagation()
        evt.preventDefault()
        evt.dataTransfer.dropEffect = 'copy'
    }

    function handleFiles(e: any) {
        e.preventDefault()
        const file: File = (e.dataTransfer || e.target).files[0]

        if (!file) return

        const ext = file.name.slice(file.name.lastIndexOf('.') + 1)

        if (!['doc', 'docx', 'pdf'].includes(ext)) return

        setForm({ ...form, file: file as any })
    }

    return (
        <Layout
            link="/careers"
            pythia={props.pythia_properties}
            container={props.body_properties}
            header={props.header_properties}
        >
            <Box className={CareerStyles['container']}>
                <Text className={CareerStyles['title']}>
                    {props.rendering_data.cover?.title}
                </Text>
                <hr />
                <RenderPage elements={props.rendering_data.content} />
                <hr />
                <Text className={CareerStyles['title']}>Application</Text>
                <form onSubmit={submit}>
                    <Text aria-required className={CareerStyles['label']}>
                        Resume
                    </Text>
                    <Box
                        className={CareerStyles['drop-files']}
                        borderColor={
                            // errors.file
                            // ? '#E53E3E'
                            form.file ? '#5319e7' : 'silver'
                        }
                        onDragOver={handleDragOver}
                        onDrop={handleFiles}
                    >
                        <input
                            ref={fileInputRef}
                            onChange={handleFiles}
                            type="file"
                            accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf"
                            style={{ display: 'none' }}
                        />
                        <Text>
                            {form.file
                                ? (form.file as any).name
                                : 'Drag and drop to add your resume'}
                        </Text>
                        <Button onClick={() => fileInputRef.current?.click()}>
                            Browse files
                        </Button>
                    </Box>
                    <Text className={CareerStyles['heading']}>
                        Personal information
                    </Text>
                    <Box className={CareerStyles['input-group']}>
                        <Box>
                            <Text className={CareerStyles['label']}>
                                First name{' '}
                                {!form.firstname.length ? <span>*</span> : null}
                            </Text>
                            <Input
                                onKeyDown={handleTextInput}
                                onChange={handleChange}
                                onPaste={(e) => e.preventDefault()}
                                name="firstname"
                                value={form.firstname}
                                isInvalid={!!errors.firstname?.length}
                            />
                            <Text className={CareerStyles['error']}>
                                {errors.firstname}
                            </Text>
                        </Box>
                        <Box>
                            <Text className={CareerStyles['label']}>
                                Last name{' '}
                                {!form.lastname.length ? <span>*</span> : null}
                            </Text>
                            <Input
                                onChange={handleChange}
                                onKeyDown={handleTextInput}
                                onPaste={(e) => e.preventDefault()}
                                name="lastname"
                                value={form.lastname}
                                isInvalid={!!errors.lastname?.length}
                            />
                            <Text className={CareerStyles['error']}>
                                {errors.lastname}
                            </Text>
                        </Box>
                    </Box>
                    <Box className={CareerStyles['input-group']}>
                        <Box>
                            <Text className={CareerStyles['label']}>
                                Email{' '}
                                {!form.email.length ? <span>*</span> : null}
                            </Text>
                            <Input
                                isInvalid={!!errors.email?.length}
                                onChange={handleChange}
                                name="email"
                                value={form.email}
                                type="email"
                            />
                            <Text className={CareerStyles['error']}>
                                {errors.email}
                            </Text>
                        </Box>
                        <Box>
                            <Text className={CareerStyles['label']}>
                                Phone number{' '}
                                {!form.phone.length ? <span>*</span> : null}
                            </Text>
                            <Input
                                onChange={handleChange}
                                onKeyDown={handlePhoneInput}
                                name="phone"
                                value={form.phone}
                                isInvalid={!!errors.phone?.length}
                            />
                            <Text className={CareerStyles['error']}>
                                {errors.phone}
                            </Text>
                        </Box>
                    </Box>
                    <Text className={CareerStyles['label']}>Message</Text>
                    <Textarea
                        value={form.message}
                        onChange={handleChange}
                        name="message"
                    />
                    <Button
                        type="submit"
                        className={CareerStyles['submit-button']}
                    >
                        Submit application
                    </Button>
                </form>
            </Box>
        </Layout>
    )
}

export async function getStaticPaths() {
    try {
        const list = await getCareersList()
        const paths = list.map(({ id }) => ({ params: { id } }))

        return {
            paths,
            fallback: false,
        }
    } catch (e) {
        return {
            paths: [],
            fallback: false,
        }
    }
}

export const getStaticProps: GetStaticProps = async (
    context
): Promise<{ props: PageData }> => {
    const { id } = context.params as { id: string }
    const data = await getCareer(id)

    return {
        props: data,
    }
}

export default Career
