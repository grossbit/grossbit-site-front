import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'
import Backend from 'i18next-http-backend'
import detector from 'i18next-browser-languagedetector'

export default i18next
    .use(detector)
    .use(Backend)
    .use(initReactI18next)
    .init({
        backend: {
            loadPath: '/locales/{{lng}}/translation.json',
        },
        load: 'currentOnly',
        react: {
            useSuspense: false,
        },
        interpolation: {
            escapeValue: true,
        },
        saveMissing: true,
        ignoreJSONStructure: true,
        fallbackLng: 'en',
    })
