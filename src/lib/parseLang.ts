import i18next from 'i18next'

function parseLang(lang: string = i18next.language) {
    if (lang?.includes('-')) {
        return lang.slice(0, lang.indexOf('-')).toLowerCase()
    }
    return lang?.toLowerCase() || 'en'
}

export default parseLang
