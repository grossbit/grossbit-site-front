import { emailRegex, linkRegex } from '../constants/regex'

export const isEmail = (email: string) => emailRegex.test(email)

export const isLink = (link: string) => linkRegex.test(link)
