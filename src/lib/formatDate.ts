function formatDate(d: any) {
    const date = d ? new Date(d) : new Date()
    const hours = date.getHours()
    const minutes = date.getMinutes()
    const day = date.getDate()
    const year = date.getFullYear()
    const seconds = date.getSeconds()
    const month = date.getMonth()

    return {
        day: day < 10 ? `0${day}` : day,
        hours: hours < 10 ? `0${hours}` : hours,
        minutes: minutes < 10 ? `0${minutes}` : minutes,
        seconds: seconds < 10 ? `0${seconds}` : seconds,
        month: month < 10 ? `0${month}` : month,
        year,
    }
}

export default formatDate
