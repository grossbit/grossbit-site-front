import Color from '../types/Color.type'

function formatColor({ r, g, b, a }: Color) {
    return `rgba(${r}, ${g}, ${b}, ${a || 1})`
}

export default formatColor
