if (!self.define) {
    let s,
        e = {}
    const i = (i, t) => (
        (i = new URL(i + '.js', t).href),
        e[i] ||
            new Promise((e) => {
                if ('document' in self) {
                    const s = document.createElement('script')
                    ;(s.src = i), (s.onload = e), document.head.appendChild(s)
                } else (s = i), importScripts(i), e()
            }).then(() => {
                let s = e[i]
                if (!s)
                    throw new Error(`Module ${i} didn’t register its module`)
                return s
            })
    )
    self.define = (t, a) => {
        const n =
            s ||
            ('document' in self ? document.currentScript.src : '') ||
            location.href
        if (e[n]) return
        let c = {}
        const r = (s) => i(s, n),
            o = { module: { uri: n }, exports: c, require: r }
        e[n] = Promise.all(t.map((s) => o[s] || r(s))).then((s) => (a(...s), c))
    }
}
define(['./workbox-1846d813'], function (s) {
    'use strict'
    importScripts(),
        self.skipWaiting(),
        s.clientsClaim(),
        s.precacheAndRoute(
            [
                {
                    url: '/_next/static/chunks/346-9ca069dcb9f52837.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/433-7fedce2f06b003dd.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/612-18b2fd8228768c42.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/702-f76d9f7eb129b3ce.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/842-a1b4c2176cc27a54.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/876-4cdbac068d649253.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/framework-91d7f78b5b4003c8.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/main-8acdf038c9c76025.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/404-c40e82cb0a9afa5f.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/_app-5ae3b6b5bcf7f9d2.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/_error-2280fa386d040b66.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/about-bac5d4a6b7668884.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/careers-cd0bf1a4c180bb66.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/careers/%5Bslug%5D-10d41d095bad0b49.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/index-cf179021b9ccc52e.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/order-2b6664003210bb34.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/portfolio-0440c4dbe05be4bc.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/portfolio/%5Bname%5D-3f3b7ed7fd91b4ef.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/services-263ff050f58c3d13.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/services/outsource-58a52e99d7748e09.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/services/product-82672de5eb2f2579.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/services/product-saving-ad6bca95c24354eb.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/pages/services/product-upgrade-3eb7472dc71da142.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/polyfills-5cd94c89d3acac5f.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/chunks/webpack-fec8acd75be94c2e.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/0b9f01f6e56eeff4.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/7628b8d04baa910d.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/7c93f348907b10d3.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/7ce46ec15b949ab7.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/988ac428ca895e2d.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/a6afcf721b360259.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/ab2a03c739e4b1b5.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/be6e82a89137e13d.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/c1ff80782fdc7457.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/d4c7065c232f0d18.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/css/f6cdf9eccc0609d4.css',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/y9Yz9mYizVRtIxmrAsjL8/_buildManifest.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/y9Yz9mYizVRtIxmrAsjL8/_middlewareManifest.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/_next/static/y9Yz9mYizVRtIxmrAsjL8/_ssgManifest.js',
                    revision: 'y9Yz9mYizVRtIxmrAsjL8',
                },
                {
                    url: '/assets/5sim.webp',
                    revision: '542a0ba0aa6eddcd676dd0ac453ab042',
                },
                {
                    url: '/assets/android.png',
                    revision: '73de29b331b9053e501f71b5a4c74ccf',
                },
                {
                    url: '/assets/angular.png',
                    revision: 'b43568bc230e07ae11b2f25e67658200',
                },
                {
                    url: '/assets/bot-avatar.gif',
                    revision: '6b4df2d0d83ac0935e9963d32d03e086',
                },
                {
                    url: '/assets/docker.png',
                    revision: 'cde17fc546aea7d41499ab92467ad816',
                },
                {
                    url: '/assets/ios.png',
                    revision: '0de4345e0326d8519f59d7a8af26519f',
                },
                {
                    url: '/assets/javascript.png',
                    revision: '5bd871eabc3ab3b9a2013112bce1319e',
                },
                {
                    url: '/assets/key-product.png',
                    revision: 'b1bd883f44a552d3b25eb0d149a2b512',
                },
                {
                    url: '/assets/logo-black.png',
                    revision: '59ac36a39077c66f72c85cea9b846988',
                },
                {
                    url: '/assets/logo.jpg',
                    revision: '6f34d787808285196f831f3414bdaf4a',
                },
                {
                    url: '/assets/logo.png',
                    revision: '92c418c86697dd77f179673b20602c95',
                },
                {
                    url: '/assets/modern.png',
                    revision: 'dfa1aa6d7e3c5eb039fb8a010b00ab70',
                },
                {
                    url: '/assets/month.svg',
                    revision: '049572b10b4d9dc97494f604bdfdc294',
                },
                {
                    url: '/assets/nodejs.png',
                    revision: '350ea377dddd525f530aa988675b82a5',
                },
                {
                    url: '/assets/outsource.png',
                    revision: '973c06a667e4a9ef0704e61f00718054',
                },
                {
                    url: '/assets/react.png',
                    revision: 'a965a50be09e9722d0503a1da81d03d9',
                },
                {
                    url: '/assets/save.png',
                    revision: '052aad8b6cf9b78c71890628bed2f279',
                },
                {
                    url: '/assets/services.png',
                    revision: '2defe03b5faa2a917577963e2760d29e',
                },
                {
                    url: '/assets/sun.svg',
                    revision: 'e214f77fd497c0b12ac4b0e06d252544',
                },
                {
                    url: '/assets/typescript.png',
                    revision: 'db542ba5f575174813c6ffdb857c5b6e',
                },
                {
                    url: '/assets/vacancy-bg.jpg',
                    revision: '2b84493ef05fed235c29be4e6e772ed6',
                },
                {
                    url: '/assets/vue.png',
                    revision: '36e272232ff2f8fbbc8c7e4b3a674a61',
                },
                {
                    url: '/assets/web.png',
                    revision: '8d120e29212f0e89b0d8cabf2f8b8639',
                },
                {
                    url: '/favicon.ico',
                    revision: 'c30c7d42707a47a3f4591831641e50dc',
                },
                {
                    url: '/favicon.jpg',
                    revision: '6bd955eca2fc953c937caae47a09b773',
                },
                {
                    url: '/locales/en/translation.json',
                    revision: '5b0fb21d0ba1e419de3b849258779351',
                },
                {
                    url: '/locales/ru/translation.json',
                    revision: '07247353332c4b24d672aa6f4864f2d0',
                },
                {
                    url: '/manifest.json',
                    revision: '4b08bd72d4aac5b9951a56ec0549e381',
                },
                {
                    url: '/vercel.svg',
                    revision: '4b4f1876502eb6721764637fe5c41702',
                },
            ],
            { ignoreURLParametersMatching: [] }
        ),
        s.cleanupOutdatedCaches(),
        s.registerRoute(
            '/',
            new s.NetworkFirst({
                cacheName: 'start-url',
                plugins: [
                    {
                        cacheWillUpdate: async ({
                            request: s,
                            response: e,
                            event: i,
                            state: t,
                        }) =>
                            e && 'opaqueredirect' === e.type
                                ? new Response(e.body, {
                                      status: 200,
                                      statusText: 'OK',
                                      headers: e.headers,
                                  })
                                : e,
                    },
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /^https:\/\/fonts\.(?:gstatic)\.com\/.*/i,
            new s.CacheFirst({
                cacheName: 'google-fonts-webfonts',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 4,
                        maxAgeSeconds: 31536e3,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /^https:\/\/fonts\.(?:googleapis)\.com\/.*/i,
            new s.StaleWhileRevalidate({
                cacheName: 'google-fonts-stylesheets',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 4,
                        maxAgeSeconds: 604800,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\.(?:eot|otf|ttc|ttf|woff|woff2|font.css)$/i,
            new s.StaleWhileRevalidate({
                cacheName: 'static-font-assets',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 4,
                        maxAgeSeconds: 604800,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\.(?:jpg|jpeg|gif|png|svg|ico|webp)$/i,
            new s.StaleWhileRevalidate({
                cacheName: 'static-image-assets',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 64,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\/_next\/image\?url=.+$/i,
            new s.StaleWhileRevalidate({
                cacheName: 'next-image',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 64,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\.(?:mp3|wav|ogg)$/i,
            new s.CacheFirst({
                cacheName: 'static-audio-assets',
                plugins: [
                    new s.RangeRequestsPlugin(),
                    new s.ExpirationPlugin({
                        maxEntries: 32,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\.(?:mp4)$/i,
            new s.CacheFirst({
                cacheName: 'static-video-assets',
                plugins: [
                    new s.RangeRequestsPlugin(),
                    new s.ExpirationPlugin({
                        maxEntries: 32,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\.(?:js)$/i,
            new s.StaleWhileRevalidate({
                cacheName: 'static-js-assets',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 32,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\.(?:css|less)$/i,
            new s.StaleWhileRevalidate({
                cacheName: 'static-style-assets',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 32,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\/_next\/data\/.+\/.+\.json$/i,
            new s.StaleWhileRevalidate({
                cacheName: 'next-data',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 32,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            /\.(?:json|xml|csv)$/i,
            new s.NetworkFirst({
                cacheName: 'static-data-assets',
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 32,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            ({ url: s }) => {
                if (!(self.origin === s.origin)) return !1
                const e = s.pathname
                return !e.startsWith('/api/auth/') && !!e.startsWith('/api/')
            },
            new s.NetworkFirst({
                cacheName: 'apis',
                networkTimeoutSeconds: 10,
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 16,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            ({ url: s }) => {
                if (!(self.origin === s.origin)) return !1
                return !s.pathname.startsWith('/api/')
            },
            new s.NetworkFirst({
                cacheName: 'others',
                networkTimeoutSeconds: 10,
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 32,
                        maxAgeSeconds: 86400,
                    }),
                ],
            }),
            'GET'
        ),
        s.registerRoute(
            ({ url: s }) => !(self.origin === s.origin),
            new s.NetworkFirst({
                cacheName: 'cross-origin',
                networkTimeoutSeconds: 10,
                plugins: [
                    new s.ExpirationPlugin({
                        maxEntries: 32,
                        maxAgeSeconds: 3600,
                    }),
                ],
            }),
            'GET'
        )
})
